---
title: "12. Special Events"
weight: 12
draft: false
---
## 12.1 Week E^0
### 12.1.1 General

This policy governs the Week E^0 events and operations. The purpose of Week E^0 is to welcome first year engineering students to UBC, to foster pride in our school and faculty, and to introduce first years to their classmates and to upper year students. 

### 12.1.2 Rules
To uphold the purpose of Week E^0, all EUS volunteers must adhere to the following rules. Rules will be communicated to all volunteers as they sign up. 
1. Volunteers shall remain sober while participating in official Week E^0 activities. 
1. Volunteers must adhere to the Orientations Student Leader statement as well as the University’s Non-Academic Misconduct Policy.
1. Volunteers must participate in all training as required by the EUS Executive. 
1. Volunteers will ensure that they demonstrate respect for first year students at all times. 
1. Any volunteers displaying offensive and/or inappropriate behaviour may be removed from their position, without warning, upon agreement by the Executive. 
1. The Executive may create additional rules as necessary. 

### 12.1.3 Organization
The organization of Week E^0 is coordinated by the Week E^0 Director who reports to the VP Spirit and EUS Executive.  The duties of the Week E^0 Director is outlined in the EUS Volunteer Manual

### 12.1.4 Events
1. All volunteers and first year students must respect the community and environment they are in, especially during off campus events. 
1. Off Campus Events that may be held during Week E^0 include:
    1. Week E^0 Retreat

### 12.1.5 Miscellaneous
1. Week E^0 Kits
    1. Kits are handed out on Imagine Day as coordinated with the Faculty’s Student Development Officer. 
    1. Kits include but are not limited to:
        1. The Week E^0 t-shirt
        1. The EUS Handbuk 
        1. nEUSpaper

## 12.2 E-WEEK
### 12.2.1 General
This policy governs the E-WEEK events and operations. The purpose of E-WEEK is to encourage healthy competition, to foster pride in our school and faculty, and to promote relationships between all students in engineering. 

The rules, events and organization of E-WEEK are outlined in the E-WEEK Guidebook . The E-WEEK Guidebook will be approved annually by EUS Council, before the end of term one.

### 12.2.2 Organization
The organization of E-WEEK is the responsibility of the E-WEEK Director, who reports to the VP Spirit and EUS Executive.  The duties of the E-WEEK Director are outlined in the EUS Volunteer Manual

### 12.2.3 Teams
All program and ex-officio clubs are welcome to participate in E-WEEK. The teams must be completely comprised of current EUS members. 

### 12.2.4 Engineer’s Ball 
The Engineer’s Ball shall be the closing event of E-WEEK. The Ball shall be a formal affair to congratulate the winning teams and thank various volunteers. E-WEEK Awards shall be given out at Engineers Ball along with other EUS Awards as described in Section 11.1.

### 12.2.5 E-WEEK Awards
During E-WEEK multiple awards will be given out at the Engineer’s Ball. They are:
1. E-WEEK Cup
    1. This is awarded to the winner of E-WEEK. 
1. E-WEEK Spirit Award
    1. This is awarded to a team who showed outstanding spirit, sportsmanship and dedication throughout E-WEEK.

## 12.3 Iron Pin Ceremony
### 12.3.1 General
The EUS Iron Pin Ceremony shall have the following purposes:
1. To build a sense of community and collective identity among students, staff, and faculty
1. To create a common set of values and expectations
1. To set a standard for professional and ethical behaviour
1. To tie conduct at the university to conduct in industry and beyond

By taking part in the EUS Iron Pin Ceremony, participants shall: 
1. Be formally welcomed into the UBC Engineering (Vancouver) community by creating a tangible connection.
1. Learn and actively accept the UBC Engineering Code of Ethics
1. Understand how the UBC Engineering Code of Ethics is linked to the APEGBC Code of Ethics and professional practice

### 12.3.2 Organization 
The EUS shall have authority over all aspects of the EUS Iron Pin Ceremony.
The EUS shall task the EUS Iron Pin Manager to organize the Iron Pin Ceremony.
The EUS Iron Pin Manager shall:
1. Oversee the organization of the Iron Pin Ceremony
1. Ensure that the goal of the Iron Pin Ceremony are being met
1. Run the Ceremonies within the Budget set out by EUS Council 

### 12.3.3 Types of Pins
1. Year Pins
    1. Year Pins are given to Incoming UBC Vancouver Engineering Students in their First Term (Fall Term). 
    1. Graduate Students, Staff and Faculty are also given a Year Pin for the year they started their terms at UBC Vancouver Engineering. 
    1. The year on the pin will be the year in which the Fall Term occurs. 
    1. The first Year Pin to be given out was 2014. 
1. Supporter Pin
    1. Supporter Pins are given to anyone who is not a current student, staff or Faculty of UBC Vancouver Engineering but are a supporter of the UBC Vancouver Engineering Community and the UBC Engineering Undergraduate Society. 
1. Founder Pin
    1. Founder Pins were only given out in the 2014/15 Academic Year. These were given to all students, staff and faculty who were not in their first year of UBC Vancouver Engineering. 
    1. The Founder Pins were to symbolize the founding of the Iron Pin Ceremony. 
    1. Founder pins should never be re-ordered. 

### 12.3.4 Ceremony Scheduling
The Iron Pin Ceremony shall take place twice per year. 
1. The Year Pin Iron Pin Ceremony shall take place once per year in the last weeks of Term One in the Winter Session. This ceremony shall be open to all students, staff and Faculty in their first term of UBC Vancouver Engineering. 
1. The Supporter Pin Iron Pin Ceremony shall take once per year during second term, in conjunction with a Year Pin Ceremony for those who could not attend the first.

The EUS Executive must approve any additional Iron Pin Ceremonies. Requests for additional Iron Pin Ceremonies must be submitted to the EUS President at least two weeks prior to the requested date. 

Iron Pin Ceremony Details can be found in the Iron Pin Manual.
