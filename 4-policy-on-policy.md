---
title: "4. Policy on Policy"
weight: 4
draft: false
---

## 4.1 General 
1. The Policy Manual is a supporting document to the EUS Constitution. No part of this Policy Manual may be interpreted in a sense contrary to the EUS Constitution.
1. Policy amendments require a two-thirds (2/3) in-favour vote by the Board of Governors for adoption.
1. No provisions of Policy may be suspended except by two-thirds (2/3) Resolution of Council. When Council suspends a provision or provisions of policy, such suspension shall only be in effect for the duration of the meeting at which it is made unless Council, by two-thirds (2/3) Resolution, sets some other fixed period of time for the suspension. 
1. Changes to the Policy Manual’s structure, numbering, and format are not considered  policy amendments, and require a simple majority vote by the Board of Governors for adoption.

## 4.2 Timeline
1. Proposed additions or changes to policy must be provided to the VP Administration before the circulation of the Council Agenda.
1. The proposed policy changes,  including a brief preamble outlining the purpose of the addition or change, will be distributed to Council with the following Council agenda for the First Reading. 
1. Council will conduct a First Reading discussion of the additions or changes at the following Council Meeting
1. The next Council Meeting, the updated policy changes will be circulated with the Council agenda and Council will conduct a Second Reading discussion of the additions or changes.
1. After the Second Reading has occurred, Council may vote on the changes at that meeting. 
1. Amendments to the proposed changes can be made at any time during the discussion.
1. The Policy Manual will be considered amended as of the date of the most recent addition or change to its contents. It will be dated accordingly.
1. The mover of the motion shall be recorded as the name for the change.