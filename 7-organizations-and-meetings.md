---
title: "7. Organizations and Meetings"
weight: 7
draft: false
---

## 7.1 EUS Board of Governors 
### 7.1.1 Composition
The Board of Governors shall consist of the following:

1. The Executive Officers
1. The Program Clubs Presidents
1. The First Year President

### 7.1.2 Removal
The removal policy for a member of the Board of Governors who is an Executive Officer is outlined in Removal of Elected Officials 9.2.5.

The removal policy for a member of the Board of Governors who is not an Executive Officer is for any of the following reasons:

1. Resignation
1. Absence from two consecutive Council Meetings, without providing a proxy and without justification deemed valid by the Board of Governors
1. Absence from three consecutive Council Meetings
1. Unsatisfactory performance according to a petition signed by three-quarters of the Board of Governors or one-quarter of the active membership represented by the Board member. 

### 7.1.3 Power and Duties
1. The Board shall exercise no control over the activities and conduct or business of any of the Program Clubs, which, although represented on the Board, operate under a separate charter from the Alma Mater Society.
1. The Board shall be the highest governing body in the Society’s structure, with the power to overturn any decision made by any of the other levels of governance, excluding those made by the general assembly of EUS members through a referendum or quorate general meeting.
1. Any decision by the Board may be overturned by a decision passed through a referendum, annual general meeting, or special general meeting.
1. The Board shall define all general policies of the Society.
1. The Board shall oversee the fiscal position of the Society, including approving a budget for each Academic Year.
1. The Board shall oversee the EUS Volunteer structure. This shall be brought forth for approval by the EUS Executives to EUS Council three times per year:
    1. At the budget meeting
    1. Once in the fall semester
    1. Once in the spring semester
1. The Board will ensure that the Executive is operating within the policies of the Society.
1. The Board shall give the Executive direction for the Academic Year.
1. Each member of the Board shall control one vote on the Board of Governors.
1. In the case of programs, each program club shall control one vote.
1. Each member of the Board shall be responsible for voting in such a way that they feel will best benefit the Society as a whole, as opposed to individual programs.
1. Each member of the Board shall be responsible for attending the appropriate meetings as described in Meeting Policy Section 7.4.

## 7.2 EUS Committees
This section elaborates on the structure, composition, and purpose of the standing EUS Committees.

### 7.2.1 General
#### 7.2.1.1 Purpose
Committees shall act as a forum for organization and discussion outside of EUS Council. 

#### 7.2.1.2 Responsibilities
1. Committees shall operate by a majority vote system.
1. Committees will be chaired by the executive member. If more than one executive sit on a committee, the chair will be decided by the executive committee.
1. Chairs may delegate their duties to another voting member of the committee by a resolution of the committee.
1. The Chair will conduct all meeting of their committee to ensure each member has time to share their views and ideas with other members of the committee. 
1. The Chair has the right to discuss removal of a committee member with the EUS Council in camera should he/she decide the committee cannot function properly with this member present. The EUS Council will decide whether a removal seems necessary and instruct the Chairperson accordingly. 
1. The Committee must take minutes of all meetings. The minutes of meeting shall include motivation for any substantive motions, recommendations, and actions discussed in meetings, along with a report of the discussions at such meetings. 
1. The Committee must approve minutes at the following meeting of that committee.
1. The Chair must submit all approved Committee minutes for the next sitting of Council after the Committee meeting at which they were approved. 
1. The Chair must submit an update to the relevant Executive member for presentation at the next sitting of Council following any Committee meetings. 
1. Quorum of committee meetings shall be one half of the sitting members, unless otherwise specified within.

#### 7.2.1.3 Membership
1. Each member on the committee shall hold a maximum of one vote.
1. In the case of Program Club Representatives, only one vote will be awarded per Program Club.
1. A voting Committee member may proxy their vote at a Committee meeting to any Active Member of the EUS. 
1. Notice of the proxy must be communicated to the Chair no less than 24 hours before the start of the meeting in question. 
1. Each person may only hold one proxy vote per meeting.

#### 7.2.1.4 Removal
1. A member of any Committees who is not an Executive Officer or Appointed Volunteer may be removed from the committee for any of the following reasons:
    1. Resignation
    1. Absence from three consecutive Committee Meetings
    1. Absence from two consecutive Committee Meeting without providing a proxy and without justification deemed valid by the Committee
    1. Unsatisfactory performance according to a petition signed by three-quarters of the Board of Governors.
1. Absences of Program Club Representatives shall:
    1. Result in decreases to Performance Funding as outlined in Performance Funding Section 10.2.3.
    1. Be reported to the Program Club President
1. Absences of Ex-Officio Club Representatives shall:
    1. Result in consequences as outlined in Ex-Officio Club Policy Section 6.2.
    1. Be reported to the Ex-Officio Club President

### 7.2.2 EUS Academic Committee
#### 7.2.2.1 Purpose
This committee shall act as a forum to discuss academic issues and coordinate all attempts to improve academic conditions. Its goal is to better facilitate communication between program academic representatives, and advise the VP Academic Affairs on faculty-wide academic issues outside of the SAC and Grand Council. The scope of this committee includes, but is not limited to: compiling a list of academic issues faced by students; suggesting improvements to academic curriculum; discussing and reviewing new academic initiatives; and coordinating academic events across programs. 

#### 7.2.2.2 Composition
The Academic Committee shall consist of: 

1. The VP Academic Affairs
1. The EUS Curriculum Director
1. One Academic Representative Per Program Club
1. And The First Year Council Academic Representative
The invitees to this committee shall be:
1. The Engineering AMS Representatives
1. The Applied Science Student Senator

#### 7.2.2.3 Meetings
This committee shall meet at least three times per semester as per the following timeline: once before the second week of the semester to set dates for cross-program academic events; and once per month thereafter to discuss academic issues and initiatives. The committee shall meet once at the end of the academic year to review the year and create recommendations for the following year. 

### 7.2.3 EUS Scholarship Committee
#### 7.2.3.1 Purpose
The Scholarship Committee shall be primarily responsible for awarding the EUS Community Contribution Award in accordance with the EUS Community Contribution Award Policy (see Appendix A), as well as assessing any other awards on an as-needed basis. 

#### 7.2.3.2 Composition
The Selection Committee shall comprise of:

1. The VP Academic Affairs
1. APSC Associate Dean Education & Professional Development, or designate,
1. APSC Student Senator, or designate,
1. And one member of the EUS Board of Directors.

If the VP Academic Affairs is also the APSC Student Senator, the APSC Student Senator seat shall be replaced with another member of the EUS Board of Directors. In the event the APSC Student Senator or the APSC Associate Dean, Education & Professional Development, neither participate themselves, nor nominate a designate, they shall be replaced with a member of the EUS Board of Directors. 

#### 7.2.3.3 Meetings
The meetings are as outlined in Appendix A. 

### 7.2.4 EUS Conferences Committee
#### 7.2.4.1 Purpose
This committee shall be responsible for all aspects of selecting and sending delegates to EUS subsidized conferences and competitions.  This committee shall set timelines for promotion and applications, approve delegate application documents, and select the Society’s delegates according to the publicized selection procedure. 

#### 7.2.4.2 Composition
The Selections Committee shall consist of:

1. The President
1. The Conferences Director 
1. The Industry and Professional Development Director
1. And two non-executive members of the Board

#### 7.2.4.3 Meetings
This committee shall meet a minimum of once during the May to August period for the purpose of setting application and selection dates. From this, they shall set committee meeting dates for the year, meeting as often as necessary to select delegates. The committee shall meet once at the end of the academic year to review the year and create recommendations for the next year.

Applications shall open as indicated in the above list, shall remain open for a minimum three-week period, and the EUS Conferences Committee shall review delegate applications within one week. 

The Conferences Committee shall review all external affiliations annually. They shall provide a report with recommendations to EUS Council regarding external affiliations prior to the end of the year.

### 7.2.5 Design Teams Committee
#### 7.2.5.1 Purpose
This committee shall act as the interface between Engineering Student Design Teams at UBC and the Engineering Undergraduate Society. The main task of this Committee will be to submit a request or requests to the EUS Executive on how to spend the EUS EDTC Fund.

#### 7.2.5.2 Composition
The Design Teams Committee shall consist of:

1. The EDTC Coordinator as Chair,
1. The EUS President or designate,
1. A representative (preferably the Captain or equivalent role) from each Engineering Student Design Team as recognized by the Faculty of Applied Science.

#### 7.2.5.3 Meetings
This committee shall meet at least once per semester.

### 7.2.6 EUS Events Committee
#### 7.2.6.1 Purpose
This committee shall be concerned with promoting events within the Society and to the community at large.  This committee is responsible for preventing event conflicts whenever possible. The committee is also responsible for creating and maintaining the EUS Events Calendar.

#### 7.2.6.2 Composition
The EUS Events Committee shall consist of:

1. The VP Student Life
1. The VP Administration
1. The Social Director 
1. One Representative from Assigned Ex-Officio Clubs, 
1. And One Social Representative from each Program Club and First Year Council. 

#### 7.2.6.3 Meetings
This committee shall meet a minimum of once during the May to August period to plan out a rough schedule of events for the year.  The committee will create an EUS Events Calendar at this time. 
The committee will meet at least once at the beginning of each term, to finalize the event schedule and complete any related contracts, liquor licenses, and payments. The VP Administration will provide contracts to all programs with upcoming events ahead of the meeting. 
The committee shall meet once at the end of the academic year to review the year and create recommendations for the next year.

### 7.2.7 ESC Governance Committee
Note that this committee will be seen as equivalent to the ESC Planning Committee, as called for in the ESC Governance Agreement, and mentioned in section 8.2. Anything written in this section shall be in no way considered contrary to the ESC Governance Agreement. 

#### 7.2.7.1 Purpose
The purpose of the Committee is outlined in the ESC Governance Agreement. Additional purposes of the Committee are outlined below:

1. To try to resolve conflicts as they arise between the EUS, the Faculty of Applied Science, and UBC related to the building who wish to use an ESC room and/or space at the same time. If an agreement cannot be reached, additional steps can be taken in accordance with section 12 of the ESC Governance Committee.
1. Advise the Dean of the Faculty concerning social space programming related to the use of functional space in the Building.
1. Develop and monitor an annual plan as called for in the ESC Governance Agreement.

#### 7.2.7.2 Composition
The mandated composition of the ESC Governance Committee is mandated in the ESC Governance Agreement.  It is recommended that the EUS members be as follows:

1. The EUS President,
1. The EUS VP Administration,
1. The EUS VP Student Life,
1. An EUS Student-at-Large, 
1. An Alumni Representative, chosen by the EUS President

#### 7.2.7.3 Meetings
The ESC Governance committee shall meet at least four times per year. Once shall be in May to present the Annual Plan, once in June to approve the Annual Plan, and once in each winter academic term to receive updates on building operations and the Annual Plan.

In addition, the governance agreement specifies the following for meetings

1. Quorum for meetings shall be three (3) Committee members, with at least two (2) student representatives and one (1) UBC representative. 
1. The Committee shall select a Chair at its first meeting each year

### 7.2.8 EUS E-WEEK Committee 
#### 7.2.8.1 Purpose
This committee shall be concerned with promoting and organizing E-WEEK. 

#### 7.2.8.2 Composition
The EUS E-WEEK Committee shall consist of:

1. The EUS VP Spirit,
1. The E-WEEK Director,
1. One Representative from each Ex-Officio Club, 
1. And One E-WEEK Representative from each Program Club and First Year Council. 

Absences from committee meetings prior to E-WEEK shall not count towards Performance Funding or Ex-Officio status. Any Program or Ex-Officio Clubs that miss two or more E-WEEK Committee meetings prior to E-WEEK, without permission from the E-WEEK Director, will be ineligible to compete in E-WEEK for that year.

#### 7.2.8.3 Meetings
The EUS E-WEEK Committee shall meet at minimum twice in Term One to finalize the events planned and the rules of E-WEEK.  The EUS E-WEEK Committee will also discuss strategies to increase awareness of E-WEEK events within the EUS and competing teams. The EUS E-WEEK Committee must submit the E-WEEK Guidebook to council for presentation before the final EUS Council meeting in Term One. 

The Committee will meet once in Term Two prior to E-WEEK, to discuss logistics of E-WEEK.

Following E-WEEK the committee will meet once to review E-WEEK as a whole. Feedback from the meeting will be used to compile the E-WEEK Feedback Report.

### 7.2.9 EUS Executive Awards Committee
#### 7.2.9.1 Purpose
This committee shall determine the recipients of the Executive Awards, which can be found in Awards Policy Section 11.1. 

#### 7.2.9.2 Composition
The Executive Awards Committee shall consist of:

1. The President as Chair, 
1. The Executive Officers, 
1. And one non-executive Board member.

#### 7.2.9.3 Meetings
This committee shall meet twice per year as needed. 

### 7.2.10 EUS Executive Committee
#### 7.2.10.1 Purpose
This committee shall be made up of Executive Officers and handle all business and operations of the Society. 

#### 7.2.10.2 Composition
The Executive Committee shall consist of:

1. The Executive Officers.

#### 7.2.10.3 Meetings
The Executive shall meet at least once every week throughout the winter academic terms and at least once every two weeks during the summer months, unless otherwise decided by a majority vote of the Executive. Meeting times will be set by the Executive Officers. The committee shall meet once at the end of the academic year with the incoming Executive to review the year and create recommendations for the next year.

### 7.2.13 EUS Sports Committee
#### 7.2.13.1 Purpose
This committee shall be concerned with promoting and organizing sports events in the EUS and program clubs. The Sports Director will communicate the relevant events to the EUS Events Committee and VP Communications as necessary. 

#### 7.2.13.2 Composition
The EUS Sports Committee shall consist of:

1. The VP Student Life
1. The Sports Director
1. Intramural Manager
1. And One Sports Representative from each Program Club and First Year Council. 

#### 7.2.13.3 Meetings
This committee shall meet a minimum of once during the May to August period to plan and schedule sports events for the year. The EUS Sports Committee shall meet once before UBC Recreation Intramurals Term One Registration (September) and once before Term Two Registration (November). The EUS Sports Committee will also be in charge of organizing the EUS involvement in Faculty Cup.  The committee shall meet once at the end of the academic year to review the year and create recommendations for the next year.

### 7.2.14 EUS Finance Committee
#### 7.2.14.1 Purpose
This committee shall be concerned with reviewing financial matters that affect the EUS and its budget. The committee is responsible for overseeing the creation of the EUS budget, reviewing the budget on a regular basis, and creating a recommended budget for the following year. The committee will oversee the approval and distribution of club funding, review EUS Club Support Fund applications before they are brought to council, and oversee the ESC Capital Purchase Fund.

#### 7.2.14.2 Composition
The EUS Finance Committee shall consist of:

1. The VP Finance
1. The VP Administration
1. Two other EUS Executives
1. Two other Board Members
1. And one Student at Large. 

#### 7.2.14.3 Meetings
The committee shall meet on a monthly basis, however may ultimately meet at will so long that the following mandates are filled:

1. Oversee the creation of the annual budget during the summer.
1. Review club funding event proposals, as per performance funding policy 10.2.
1. Review the budget and aid the VP Finance in the submission of regular budget presentations to Council.
1. Create a recommended budget for the following year.
1. Oversee the ESC Capital Purchase Fund.
1. Reviewing EUS Club Support Fund applications before bringing them to council, within two weeks of receiving an application.

### 7.2.15 Ad-Hoc Committees
#### 7.2.15.1 Purpose
Ad-Hoc committees may be struck by EUS Council as a forum for discussion and organization outside council. The mandate of the ad-hoc committee is to be set by council during the creation of the committee.

#### 7.2.15.2 Creation
Ad-Hoc Committees must be struck by a motion to council. The motion must include:

1. Mandate of the Committee
1. Composition of the Committee
1. Chair of the Committee
1. Expected meeting frequency of the Committee

#### 7.2.15.3 Dissolution
Ad-Hoc Committees may be dissolved at any time by a motion to Council. 

## 7.3 EUS Council 
### 7.3.1 Power and Duties
1. The Council shall behave as an advising body to the Executive and Board.
1. Decisions made by the Council shall be conducted in the form of straw polls. Results are non-binding and are suggestions to the Board only.  

### 7.3.2 Composition
1. The EUS Council shall consist of the following:
1. The Board of Governors
1. The Applied Science Student Senator 
1. The EUS AMS Representatives

Invited members of EUS Council are:

1. The President or Representative for each of the Ex-Officio Clubs

### 7.3.3 Removal
The removal policy for a member of EUS Council who is a Board of Governor can be found in EUS Board of Governors Section 7.1.

The removal policy for the Applied Science Student Senator is governed by the University Senate.

The removal policy for EUS AMS Representatives from AMS Council is governed by AMS Code.

The removal policy for EUS AMS Representatives from EUS Council is for any of the following reasons: 

1. Resignation
1. Absence from three consecutive Council Meetings
1. Absence from two consecutive Council Meetings, without providing a proxy and without justification deemed valid by the Board of Governors
1. Unsatisfactory performance according to a petition signed by three-quarters of the Board of Governors or one-quarter of the active membership that member of the Board represents. 

### 7.3.4 Meetings
Council Meetings shall:

1. Be held at least once every two weeks during the winter academic terms for the purpose of directing the activities of the Executive.
1. Have quorum of three-quarters of the Board.
1. Not begin until Quorum has been met.
1. Be open to all members of the Society, who are encouraged to attend.
1. Be attended by:
    1. The Council

### 7.3.5 In-Camera Meetings
In-Camera Meetings shall:

1. Be held whenever the Board requires “in camera” or offline sessions.
1. Have quorum of three-quarters of the Board
1. Not begin until Quorum has been met.
1. Be closed to all non-members of the Board of Governors, unless invited by a 2/3 vote of Board of Governors.
1. Be attended by:
    1. The Board of Governors


## 7.4 EUS Meetings 
### 7.4.1 Annual General Meetings
Annual General Meetings shall:

1. Be held at least once each year, during E-WEEK.
1. Have Quorum of one-twentieth of the active members.
1. Be able to pass Constitutional Changes if Quorum is met.
1. Be open to all members of the Society, who are encouraged to attend.
1. Be attended by:
    1. The Council
    1. Directors
    1. Managers
    1. Representatives

### 7.4.2 Extraordinary Meetings
Extraordinary Meetings may be called by the EUS President:

1. At the request of the Board
1. Not less than three days, and no more than fifteen days, after the receipt of a petition signed by 20% or more of the Active Members of the Society.

Extraordinary Meetings shall:

1. Have quorum of one-twentieth of the active members.
1. Pass Constitutional Changes if quorum has been met.
1. Open to all members of the Society, who are encouraged to attend

Be attended by:

1. The Council
1. Directors
1. Managers
1. Representatives

### 7.4.3 Council Strategic Planning Meeting
Council Strategic Planning Meeting shall:

1. Be held once a year following the completed elections of all members of the Executive and Board.
1. Be open to active members of the Society by invitation only.
1. Have Quorum of three-quarters of the Board.
1. The Council Strategic Planning Meeting shall be used to:
    1. Familiarize the attending individuals with each other.
    1. Familiarize members with the procedures of Council and to pass along any relevant items or information.
    1. Provide the Executive with direction over the summer.
    1. Set an overarching goal for the Society for the upcoming year.
1. The Council Strategic Planning Meeting shall be attended by:
    1. The incoming Council.
    1. The outgoing Council.

### 7.4.4 Grand Council
Grand Council Meetings shall: 

1. Be held at least once each winter academic term.
1. Be held for the purpose of general consultation and planning in preparation for the following BASC Student Advisory Council meeting.
1. Be open to all members of the Society, who are encouraged to attend.
1. Be attended by:
    1. The Board of Governors
    1. All Program Club Year Reps and/or Academic Reps
    1. EUS Academic Committee