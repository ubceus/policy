---
title: "Policy"
date: 2017-10-16T19:41:50-07:00
draft: false
chapter: true
---

# The EUS Policy

This is the policy manual of the UBC Engineering Undergraduate Society. It is controlled by the EUS Board of Governors. Any questions, comments, or clarifications should be directed to [vpadmin@ubcengineers.ca](mailto:vpadmin@ubcengineers.ca).
