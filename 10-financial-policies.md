---
title: "10. Financial Policies"
weight: 10
draft: false
---

## 10.1 Budget
### 10.1.1 Description
1. The EUS Budget shall be the guiding document for the Societies’ spending and direct the Executive for the priorities of the Society. 

### 10.1.2 General
1. The EUS VP Finance shall be responsible for the creation and monitoring of the EUS Budget as outlined in Executive Officer Duties Section 5.1.6.
1. The EUS Budget can only be amended by the EUS Board of Governors by a vote at any point during the year. 

### 10.1.3 Timeline
1. The EUS VP Finance shall create the incoming budget during the Summer Term for approval by the deadline set by the AMS. 
1. The EUS Board shall meet once prior to the deadline set by the AMS to approve the incoming budget. The approval authorizes the Executive to make necessary expenditures outlined in the budget without further approval.
1. The EUS VP Finance shall present updated numbers to the Board as outlined in Executive Officer Duties Section 5.1.6. 
1. Expenditures not outlined in the budget must be approved according the EUS Sponsorship Policy Section 10.4.
1. Allocation of EUS Funds must follow the procedures outlined in the EUS Funds Policy Section 10.3.
1. The EUS VP Finance shall present a summary of the budget and final numbers at the EUS Strategic Planning Meeting. 

## 10.2 Club Funding
The EUS provides recognized program clubs with access to yearly funding. Each program is guaranteed base funding, while event funding and performance funding require club's meet certain criteria for funding.

### 10.2.1 Base Funding
1. Base Funding per program club will equal $5 per student registered in their respective program club.
1. Base Funding is guaranteed to the program clubs.
1. Base Funding will be distributed no later than one week following the EUS's receipt of term one student fees.

### 10.2.2 Event Funding
1. The total Event Funding be $3.50 per student recognized by a program club.
1. The total event fund will be divided equally between recognized program clubs.
1. Clubs must submit an event proposal to the EUS Finance Committee to access their funds.
1. Finance Committee will recognize proposals for the following event types:
    1. Social Events
    1. Academic Events
    1. Industry & Professional-Development Events
1. Proposals must include:
    1. Event type
    1. Event description & purpose
    1. Event budget, including expected EUS Events Fund Contribution
    1. Event planning timeline
    1. Target audience
    1. Expected attendance
1. Clubs may access up to one third (⅓) of the event funding per event type. If a club wishes to access more than one third (⅓) for one event type, they must specify which other event type will have reduced funding. The club must, through the submission of an event proposal, show that they will still offer events in the other type, and justify that the event(s) will remain feasible with reduced funding.
1. Finance Committee will accept all proposals, contingent that all inclusions are complete and fairly justified.
1. Finance Committee will meet once in August to approve all event proposals received during the summer. Any term one proposals received after the meeting will be assessed within a month of submission. Finance Committee will meet once in December to approve all term two event proposals. Any term two proposals received after the meeting will be assessed within a month of submission.
1. Any event funds remaining after the term two disbursements will be transferred to the Club Support Fund.

### 10.2.3 Performance Funding
1. Performance Funding will total $3.50 per student recognized by a program club.
1. The total performance fund will be divided equally between recognized program clubs.
1. The fund will be distributed once per term, following a performance funding meeting.
1. Meetings will occur no later than five (5) business days before the end of term.
1. Meetings will be attended by the EUS VP Finance and VP Administration, and the program club President.
1. Funds will be disbursed before the last day of the term.
1. Any performance funds remaining after the term two disbursements will be transferred to the Club Support Fund.
1. Funds will be disbursed based on completion of the following tasks. If a task is partially completed, the task will only be funded the percentage of the task completed. Each task is worth a percentage of total funding available, as listed:
    1. Club Communications	15%
        1. Should include information on upcoming events relevant to the Program Club and/or engineering at large.
        1. Accepted communication includes newsletters, sent by email or made available online; or regular social media posts on multiple social media platforms.
        1. At least seven (7) newsletters or fifteen (15) posts per term constitute full funding.
        1. One half (½) of the funding will be available per term.
        1. Communications must be submitted to the EUS VP Administration, through an online system, as prescribed at the annual SPM.
    1. Club Meetings	15%
        1. Club Meetings shall be held weekly during the term. Four (4) meetings may be missed per term, meetings cannot be missed two (2) weeks in a row.
        1. The standard meeting time and location must be communicated to both club members and the EUS VP Administration, who are invited to attend.
        1. If a standard time cannot be made, the time and location of the next meeting must be included in the minutes of the previous.
        1. Meeting minutes must be submitted to the EUS VP Administration, through an online system, as prescribed at the annual SPM.
        1. Nine (9) meetings per term constitute full funding.
        1. One half (½) of the funding will be available per term.
    1. EUS Meeting Attendance
        1. Attendance requirements are outlined in Section 7.2
        1. EUS Council	20%
        1. EUS Committees
            1. Academic Committee, including SAC and Grand Council	5%
            1. Events Committee	5%
            1. Sports Committee	5%
            1. E-WEEK Committee, following E-WEEK	5%
        1. APSC/EUS Student Leader Professional Development Day	5%
        1. One half (½) of the Council and Committee funding, with the exception of E-WEEK Committee, will be available per term.
1. Graduation 	10%
    1. Provide EUS Graduation Director with club Grad Rep contact information
    1. Submit Iron Ring sizes by the deadline.
    1. Submit graduation headshots by the deadline.
    1. Submit yearbook media by the deadline.
    1. Submit Iron Ring ceremony volunteer information by the deadline.
    1. All deadlines will be communicated to department Grad Reps once they have been established by Camp 5, and communicated to the EUS.
1. Club Materials	15%
    1. Submit a copy of the club constitution and governing documents to the EUS VP Administration, once per year, for archiving.
    1. Submit a club executive contact list to the EUS VP Administration by the beginning of term one. Submit updated contact lists, if necessary, within a week of new executive appointments.
    1. Submit an annual budget for review at each performance funding meeting. Budget must include detailed expenditures for any events funded through the Events Fund, section 10.2.2.

## 10.3 Funds
### 10.3.1 Description
This policy shall govern the Society's funds. 

### 10.3.2 EUS Club Support Fund
1. The EUS Club Support Fund exists primarily for the purpose of aiding program clubs in dire need of financial assistance due to extenuating circumstances that would very negatively affect the club’s financial stability. 
1. The Club Support Fund will not fund:
    1. Professional activities, curricular projects, or conferences.
    1. Anything where emergency funding should be covered by insurance costs or repairs of a building by the Faculty, Department, or Program. 
    1. Losses that cannot be verified (e.g. undocumented cash amounts)
    1. Preventable and/or recurring incidents resulting monetary loss 
        1. This shall include, but not be limited to, pre-existing plant deficiencies being exploited (i.e. a door that doesn’t properly lock), and negligence on behalf of the department club (such as propping a door open).
1. Applications for the Club Support Fund should be submitted in writing to the EUS VP Finance and be reviewed by the EUS Finance committee before being brought to and voted on at the next Board meeting following the review. 
1. The Club Support Fund should not exceed $8000 or drop below $1000. Any additional funds will be reallocated to the EUS Budget.
1. If the total drops below $8000, it may be replenished with funds from the EUS budget, up to $1 per student in the program clubs.
1. If as of March 15 in 2017, and in any given year thereafter, the amount in the fund exceeds $1500, program clubs may apply to the fund for capital improvements, provided:
    1. The application is submitted and processed in accordance with Section 3, with the requirement that this type request to the Fund be approved by a two-thirds (⅔) majority vote of the Board.
    1. The Board is notified of funds the club(s) applying have received from this source in the three (3) preceding years, and considers this in deciding to disburse funds.
    1. The club demonstrates other possible sources of funding have been exhausted.
    1. The amount requested does not exceed one-third (⅓) of the total amount in the Fund.

### 10.3.3 EUS Engineering Design Teams Fund
1. The EUS Engineering Design Teams Fund exists to support the Engineering Design Teams and Council on joint initiatives. 
1. The EUS Engineering Design Teams Fund shall exist by the terms set out by the EDTC Referendum in 2014. 
1. Applications for the EUS Engineering Design Teams Fund shall be submitted in writing to the EUS President and be both reviewed and voted on by the Executive at the next Executive Meeting. 
1. Applications for funding may only be submitted by the Engineering Design Teams Council or recognized Engineering Design Teams. 
1. The application must benefit two or more recognized engineering design teams. 
1. If the application is accepted, the event promotions must prominently include the EUS Logo and recognition of the sponsorship. 

## 10.4 Sponsorship
### 10.4.1 Description
1. The purpose of any sponsorship provided by the EUS is to support UBC Engineering Undergraduate Students. 
1. Sponsorship is defined as financial funding for any event, activity, or program not directly organized by the EUS.

### 10.4.2 Finances
1. Sponsorship funding shall not be budgeted for in the operational budget of the EUS. 
1. Sponsorship funding will be allocated on an as-needed basis from either a projected surplus or carry-over from a previous financial year. 
1. Every year, EUS Council shall allocate a maximum of 10% of the annual EUS budget to all sponsorship proposals. The allocation of sponsorship funding requires approval by a two-thirds majority of Council. 

### 10.4.3 Eligibility
1. Any program that supports or involves UBC Engineering Undergraduate Students, with the following exceptions:
    1. Activities that can be covered by the Professional Activities Fund (PAF).
    1. Activities that are exclusive to one program.  

### 10.4.4 Procedure
1. The Sponsorship Proposal must be provided to the EUS VP Finance at least 7 days prior to the meeting at which Council will vote on the proposal.
1. If the Proposal does not fit the above eligibility criteria, the EUS VP Finance will notify the applicant of this within 48 hours of receiving the Proposal.
1. The Sponsorship Proposal will be circulated to EUS Council along with the Council Agenda prior to the Council Meeting.
1. The Sponsorship Proposal will be presented by the applicant at the next EUS Council Meeting.

### 10.4.5 Proposal
1. The Sponsorship Proposal must include the following items:
    1. Amount of funding requested.
    1. A description of the program in a maximum of two hundred words.
    1. The number of undergraduate engineering students involved or benefiting from the program - direct and indirect.
    1. Complete budget for the program.
    1. Including all secured and potential funding sources.
    1. Benefits the EUS will receive from sponsorship.
1. If the event, activity or program has been previously sponsored by the EUS, then the Proposal must include:
    1. An outline of the growth of the program and the need for continuing sponsorship in a maximum of two hundred words. 
    1. The amount previously sponsored by the EUS.
1. It is recommended that the Sponsorship Proposal includes the following items:
    1. Attached visual aids to be used in the presentation to Council.
    1. Sponsorship benefits such as:
        1. A final report that will be sent to EUS Council, outlining the success of the sponsorship. 
        1. Logo recognition

## 10.5 Sports Subsidies
### 10.5.1 Description
This policy governs the partial and full reimbursement of Sport and Recreational registration fees of teams representing the Society.

### 10.5.2 Reimbursement
1. Any current undergraduate student in engineering is eligible for EUS Sports Subsidies. Subsidies are disbursed under the discretion of the Sports Director and are awarded on a first-come first serve basis. 
1. The EUS will subsidize 40% of each member’s sports registration fee up to a maximum of $25 per student. 
1. Reimbursements will not be given for the cost of equipment, uniforms or other incidental team expenses. 
1. The Sports Director shall determine the rate of reimbursement based on the funding available and the number of applications received each term. 

### 10.5.3 Procedure
1. Students shall submit the Sports Subsidies form that can be found online. 
1. The form shall include a request for receipts. 
1. Forms and receipts must be handed in within one month of the date of the receipt. 
1. For teams, one representative from the team is responsible for filling out one form for an entire team. 
1. The Sports Director will submit a summary to the VP Finance stating the purpose and amount of the rebate along with the receipts. 
1. Money owed will be collected from the AMS Business Office.