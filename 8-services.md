---
title: "8. Services"
weight: 8
draft: false
---

## 8.1 Conferences
The following section highlights EUS conference types, applications, the selection of delegates, delegate expectations and costs associated. EUS recognized conferences and number of delegates sent are subject to change by EUS Council. EUS Council will dictate this when the EUS budget is set for the year.

### 8.1.1 Conference Types
1. Open conferences are conferences that any general member of the EUS is encouraged to apply for. Delegate applications and selection process are outlined in Applications Section 8.1.2 and Selection Section 8.1.3 by the EUS Conferences Committee Section 
1. Closed conferences are conferences that are restricted to the President and/or Director of Conferences and/or another council member at the discretion of the EUS Conferences Committee.
1. Other conferences that are not listed above can apply for funding according to the EUS Sponsorship Policy Section 10.4.

### 8.1.2 Applications
The following section explains the steps for applications to conferences.

##### Applications for open conferences shall:
1. Be open for a minimum of two weeks,
1. Be publicized in the e-nEUS,
1. Be circulated with a description of the conference, expectations and costs.

##### Applications for closed conferences shall:
1. Be submitted by eligible and willing members to the EUS Conferences Committee. 

### 8.1.3 Selection
The following section explains the steps for selection of participants.

##### Consideration Requirements
1. All applicants must be Active Members of the EUS.
1. Applicants can be from any year and discipline.
1. Information on the application forms is confidential and shall only be used for registration and contact information. The use of this information for any other purpose is prohibited.

##### Selection Process
1. All applications are to be considered equally.
1. After the application deadline, a meeting of the EUS Conferences Committee shall be called.
1. All decisions of the committee must be completed by the published dates; the decisions are final.
1. If possible, at least 2 alternate applicants should be chosen for the event in the case of withdrawals.
1. The EUS Conferences Committee shall also select a Head Delegate.

### 8.1.4 Expectations
The following sections include the expectations of the EUS for those chosen to participate in events.

#### 8.1.4.1 Code of Conduct
1. All delegates selected to attend are to conduct themselves in a professional and courteous manner. 
1. All delegates are expected to attend all sessions at the conference.

#### 8.1.4.2 Reporting Requirements
1. Delegates are required to submit reports detailing information learned, people met, and interesting and important facts.
1. As a general rule, the length of the report should be proportional to the amount of funding received and the length of the conference.
1. Report lengths are at the final discretion of the Director of Conferences. Reporting requirements should be published along with application.
1. Reports are due no later than 2 weeks after the end of the conference.
1. Rebates for costs shall not be processed until such time as the Director of Conferences receives the reports.

#### 8.1.4.3 The Head Delegate:
1. Shall hold a meeting with all delegates and Director of Conferences prior to the conference to convey expectations and plans for the conference. 
1. Shall be expected to fulfill expectations outlined by the conference.
1. Will select delegates to attend each of the sessions put on at the conferences.
1. Will acquire receipts/invoices for all delegates, transportation and accommodation fees.

### 8.1.5 Travel
The following section details the EUS’ policy on transportation to and from conferences as well as contingency situations.

1. The type of transportation chosen should be based on the number of delegates, the distance traveled, the time constraints and the cost constraints. After these constraints are taken into consideration, the cheapest and most reasonable option should be chosen.
1. The final choice of the travel options is up to the discretion of Director of Conferences, and should be approved prior to departure.
1. Types of Travel
    1. Air travel should be considered when the distance to be traveled is typically more than 1000km.
    1. Bus transportation should be considered for travel when there are only a few (less than 3) delegates, or when there are a substantial number of delegates (more than 20). In the case of limited delegates, commercial bus lines should be considered. For a larger number of delegates, bus rentals should be considered.
    1. Privately Owned Vehicles
        1. Privately owned vehicles are not to be used unless absolutely required as liability is increased. When used, the owner and all users should be reimbursed for the cost of fuel and tolls.
        1. For compensation, the owner of the vehicle is entitled to a proportional cost of an oil change.
        1. The amount total that is reimbursable is proportional to the distance traveled.
        1. The cost of a complete oil change shall be reimbursed after 5000 km are driven.
    1. Rental Vehicles
        1. Rental vehicles are more favourable for use than privately owned vehicles for improved liability.
        1. When used, compensation is to be granted for fuel, tolls, and the complete cost of the rental.
        1. Rental vehicles shall not be used for personal use and should be picked up and returned within the shortest period of time.
        1. The highest level of insurance should be purchased at the time of the rental.
        1. It is preferable that at least one person in the rental vehicle has Canadian Auto Association (CAA, of BCAA) coverage. All persons driving the vehicle shall be covered by insurance.
    1. Other Forms of Transportation
        1. Ferries and trains, among other types of transportation, should be considered for travel to and from a conference.
        1. Costs incurred can be refunded.
1. Travel Contingencies
    1. Problems that arise during travel may be funded at the discretion of the Director of Conferences and VP Finance. 
    1. All speeding tickets are to be covered by the person who was driving the vehicle at the time of the infraction. This situation applies to both personal vehicles and rental vehicles.
    1. Actions for all other situations shall be solved at the discretion of the Director of Conferences and VP Finance with consultation of the EUS Conferences Committee.

### 8.1.6 Funding
The following section details the EUS’ policy on funding.

##### Deposit:
1. Once selected delegates are required to submit a deposit to ensure their spot. The deposit will be returned dependant on amount reimbursed and if proper representation was met.
1. Delegates must be informed of this amount along with the application and again upon selection. 
1. The deposit is due no later than one week before the scheduled departure for the event.  If the deposit is not received, their position shall be forfeit and given to an alternate. 
1. Additionally, all delegates will be required to submit a refundable damage deposit, as per registration requirements for each conference.

##### Costs:
1. Delegates are responsible for all remaining costs not funded by the EUS.
1. In order to help cover costs not covered by EUS or other sources, it is up to the discretion of the Director of Conferences and VP Finance to decide upon the appropriate level of participation fees.
1. This fee must not exceed registration expected fees and must be published along with the application. 
1. Closed conferences or open conferences where executives are required to attend shall not require a participation fee and all reasonable expenses including transport, delegate fees, accommodation and a per diem will be covered by the EUS.

## 8.2 Engineering Student Centre
### 8.2.1 General
The following policy is intended to outline the organization of the EUS Engineering Student Centre. The governance policy of the Engineering Student Centre is as outlined in the agreement signed with UBC Board of Governors which can be found as the ESC Governance Agreement. No amendments can be made to the ESC Governance Agreement. 

### 8.2.2 ESC Planning Committee
ESC Operation and Programming is governed by the ESC Planning Committee. The ESC Planning Committee is as mandated in the ESC Governance Agreement. It is equivalent to the ESC Governance Committee described in 7.2.7. 

### 8.2.4 Red Sales
Red Sales is the official store of the Society. 

Red Sales shall sell the following items but is not limited to: 
1. Engineering Red Jacket
1. Engineering Red Cardigan 
1. Various Patches 

The Red Sales Director shall operate Red Sales.

### 8.2.5 ESC Eatery
The ESC Eatery shall be the Cafe of the EUS. 

The ESC Eatery hours shall be set by the ESC Eatery Director and be made public at the Cafe and online. 

The ESC Eatery Director shall put together a proposal for selling and present to the EUS Executive at the start of the year. 

The ESC Eatery Director shall operate ESC Eatery. 

### 8.2.6 ESC Bar
The ESC Bar shall be the bar and servery for the Society.

The ESC Bar shall not sell alcohol without a valid Special Occasion Liquor License or other Liquor Licenses as outlined by the Province of British Columbia and the UBC Board of Governors. 
 
The ESC Bar Director shall operate ESC Bar. 

## 8.3 Publications
### 8.3.1 General
This is the publications policy for the Society. It sets rules and procedures for what kinds of material may or may not be produced by the Society. It does not apply to material in the Society’s Archives. It does, however, apply to any materials that are to be redistributed or reproduced from the Archives. 
### 8.3.2 Publications
The policy applies to all information-carrying materials produced by the EUS including but not limited to and within the following two categories:
1. Those that carry mastheads:
    1. nEUSpapers (newspapers)
    1. Newsletters
    1. Websites
    1. Slipsticks (Yearbooks)
    1. Handbuks (Handbooks)
1. Those that do not carry mastheads:
    1. Posters
    1. T-Shirts
    1. Stickers

### 8.3.3 Responsibility
1. All publications are the responsibility of the VP Communications, who must ensure that this Policy is adhered to.
1. Publications with mastheads must clearly attribute work to the individuals who contributed to the publication. This may include editors, journalists, photographers, or illustrators. Material that is not attributed to a specific person will be assumed to have been written by the editors, unless the editors show otherwise. Anonymous submissions should be cited accordingly.
1. No pornography.
1. The B.C. Human Rights Code, Section 7, identifies 11 bases for Publication-based discrimination. No publication of the EUS shall belittle, make or imply threats against, or incite hatred of any person or group of people based on their inclusion within a group or class defined by one of these 11 criteria. Namely: race, color, ancestry, place of origin, religion, marital status, family status, physical disability, mental disability, sex, or sexual orientation.
1. A distinction should be made between public and private persons within the UBC community:
    1. Private persons have a right to not be maliciously exposed to public humiliation or ridicule, whereas satire directed at public persons may be acceptable. It is recommended that context, appropriateness, and possible consequences are considered before satirizing public figures.
    1. Private figures include EUS Members at Large, UBC professors, and TAs.
    1. Public figures include members of the EUS Council, Executive Officers, EUS Directors , EUS Managers, EUS Representatives, Executives of other prominent student organizations, AMS Executives and other officials, and UBC Administrators. 

### 8.3.4 Violations
1. If this Policy is violated, the VP Communications shall investigate and take appropriate action. Repeated violations shall be brought to the attention of the Executive and may result in the loss of editorial privileges of the editors concerned. In the case of the VP Communications, repeated violations should result in their resignation, or the establishment of an Ad-Hoc Editorial Committee to oversee all published material until the end of their term.


