---
title: "1. Preamble"
weight: 1
draft: false
---

The Policy Manual was developed in an effort to solidify the activities and policies of the University of British Columbia Engineering Undergraduate Society. The policies documented here are intended to accompany the Constitution of the University of British Columbia Engineering Undergraduate Society.

This Policy Manual will assist in the transition of the Engineering Undergraduate Society from year to year, and will also clarify the policies and activities of the Society. The procedures for amending this document are set out in Policy on Policy Section 4.0.

Overall, this document will help the Engineering Undergraduate Society to operate more effectively and efficiently in its effort to represent and serve the undergraduate engineering student body at the University of British Columbia.

## 1.1. Updates and Changes:

| **Change** | **Date** | **Main Editor** | **Motioned** |
| :--- | :--- | :--- | :--- |
| Draft 1.0 | August 15, 2007 | Bowinn Ma | n/a |
| Draft 1.1 | September 29, 2007 | Bowinn Ma | n/a |
| Draft 1.2 | November 19, 2007 | Bowinn Ma | n/a |
| Draft 1.3 | November 27, 2007 | Bowinn Ma | n/a |
| Draft 1.4 | January 7, 2008 | Bowinn Ma | n/a |
| Draft 1.5 | January 25, 2010 | William Gallego | n/a |
| Draft 1.6 | February 21, 2011 | William Gallego | n/a |
| Amended | April 9, 2011 | Hans Seidemann | Hans Seidemann |
| Amended | November 21, 2011 | Ian Campbell, Jack Park | Ian Campbell, Jack Park |
| Amended | January 30, 2012 | Hans Seidemann | Hans Seidemann |
| Amended | March 26, 2012 | Hans Seidemann | Hans Seidemann |
| Amended | April 29, 2012 | Graham Beales | Graham Beales |
| Amended | September 28, 2012 | Graham Beales | Will McEwan |
| Amended | January 28, 2013 | Graham Beales | Ian Campbell |
| Amended | March 25, 2013 | Graham Beales | Ian Campbell |
| Amended | April 7, 2013 | Graham Beales | Ian Campbell |
| Amended | April 25, 2013 | Graham Beales | Graham Beales |
| Amended | April 25, 2013 | Graham Beales | Veronica Knott |
|  |  |  | Amendments Not Tracked |
| Draft 2.0 | April 12, 2015 | Veronica Knott | Veronica Knott |
| Passed | September 14, 2015 | Alan Ehrenholz | Alan Ehrenholz |
| Amended | October 5, 2015 | Alan Ehrenholz | Mark Bancroft |
| Amended | January 11, 2016 | Jeanie Malone | Jonathon MacIntyre |
| Amended | July 21, 2016 | Jeanie Malone | Various Councillors |
| Amended | September 29, 2016 | Bryan Starick, Jeanie Malone | Bryan Starick |
| Amended | January 9th, 2017 | Nicholas Mulvenna | Nicholas Mulvenna |
| Amended | March 27th, 2017 | Bryan Starick | Jeanie Malone |
