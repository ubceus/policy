---
title: "3. Definitions"
weight: 3
draft: false
---

## 3.1 Constitutional Definitions
In the EUS Constitution and Policy Manual, unless otherwise specified: 
1. “UBC” shall hereafter refer to “the University of British Columbia Vancouver”. 
1. “The Society” shall hereafter refer to “the Engineering Undergraduate Society of UBC”. 
1. “EUS” shall be the equivalent of “the Society” 
1. “Senate” shall hereafter refer to “the Senate of UBC Vancouver” 
1. “Members” shall hereafter refer to “EUS members” 
1. “Executive” shall be the equivalent of “Executive Officers of the Society”   
1. “The Board” shall hereafter refer to “the EUS Board of Governors ” 
1. “AMS” shall hereafter refer to “the Alma Mater Society of UBC” 
1. “Constitution” shall hereafter refer to “Constitution of the EUS” 
1. “Policy Manual” shall hereafter refer to “Policy Manual of the EUS” 
1. “Appointed Volunteers” shall hereafter refer to “Directors, Managers and Representatives of the EUS”
1. “Program Clubs” shall hereafter refer to all “Department or Program Clubs.”

## 3.2 First Year Council Definitions
1. “FYC” shall hereafter refer to “First Year Council”
1. “FY” shall hereafter refer to “First Year”

## 3.3 General Policy Definitions
1. “Vice-President of Communications” shall be the equivalent of “VP Communications”.
1. “Vice-President of Administration” shall be the equivalent of “VP Administration”.
1. “Vice-President of Finance” shall be the equivalent of “VP Finance”.
1. “Vice-President of Academic Affairs” shall be the equivalent of “VP Academic”.
1. “Vice-President of Spirit” shall be the equivalent of “VP Spirit”.
1. “Vice-President of Student Life” shall be the equivalent of “VP Student Life”.
1. “The Executive” and “The Executive Committee” shall be equivalent and refer to a committee consisting of all seven Executive Officers.
1. An “affiliation”, unless otherwise defined, shall refer to a relationship between two organizations or positions where effort shall be made to work together and communicate on business relevant to both units, but in which neither group has direct control over the other.
1. A “proxy” shall refer to a member of the Society who does not hold a seat on the committee and or council in question. 
