---
title: "5. Roles"
weight: 5
draft: false
---

## 5.1 Executive
### 5.1.1 General 
#### 5.1.1.1 Description
The Executives of the EUS shall, as a whole, be responsible for the direction and management of the EUS, subject to the directions of the EUS Board of Governors.  The Executives shall be responsible to the EUS Board of Governors, and shall act in accordance with any decisions of that body. 

#### 5.1.1.2 Duties
##### Each Executive Officer shall:
1. Be an Active Member of the Society during the term they are serving.
1. Occupy only one position within the Society and each Executive position shall be occupied by only one Active Member of the Society
1. Not occupy any other Executive position within an Ex-Officio or Program Club that holds a seat on the Board of Governors. 
1. Serve for one full year from May 1 to April 30.
1. Be expected to dedicate 20 hours or more a week to the Society during their term. 
1. Sit as a voting member on the Board of Governors.
1. Inform and consult the entire executive on matters related to their portfolio. 
1. Report their actions and decisions to Council.
1. Attend or send a proxy to all assigned Committees and Council meetings. 

##### The Executive shall:
1. Coordinate and administer the day-to-day affairs of the Society.
1. Ensure the execution of the Board’s decisions.
1. Uphold the Constitution and policies of the Society.
1. Promote the Society to both EUS Members and the community-at-large.
1. Mentor potential replacements throughout their term.
1. Train their replacements at the end of their term and provide them with any relevant files, reports, introductions, and paperwork needed for them to do their role properly.

#### 5.1.1.3 Volunteers
##### Each Executive Officer shall:
1. Oversee the Directors, Managers, and Representatives for which they are assigned, as outlined in the EUS Volunteer Manual,  and ensure that they fulfill their roles in a satisfactory manner.
1. Regularly report the activities of their assigned volunteers to the EUS Council.
1. Ensure the Directors, Managers, and Representatives for which they are responsible are properly trained.
1. Absorb, re-delegate, or otherwise be responsible where necessary for any and all duties not satisfactorily carried out by those Directors, Managers, and Representatives assigned to them. 
1. In the case where a Director, Manager, or Representative is not filled, the Executive Officer shall absorb, re-delegate, or otherwise be responsible for all duties of that position. 
1. Assign an assistant to take care of some of the duties assigned to their portfolio, as necessary.

##### The Executive shall:
1. Oversee the volunteer structure as defined by the EUS Volunteer Manual.
1. Report any changes in the volunteer structure to EUS Council three times per year as defined in Section 7.1.2.

### 5.1.2 President
#### 5.1.2.1 Description
The President shall serve as the primary representative and voice of the
Engineering Undergraduate Society, articulating the mission, vision, direction, and opinions of the Society. The President presides over all affairs of the Society. In particular, the President is tasked with managing and supporting the EUS Executive.  

#### 5.1.2.2 Duties
The duties are broken up into the following sections:

##### General:
1. Preside over the affairs of the Society.
1. Be the official spokesperson of the Society on all occasions.
1. Supervise and be familiar with the entire contents of the Policy Manual and Constitution. 
1. Enforce due observance to the Constitution.
1. Absorb, delegate, or otherwise take responsibility for any and all duties that are not adequately performed by other Executives or do not fall within any other Executives’ portfolios.
1. Report to EUS Council on the affairs of the EUS and on issues related to the portfolio of the President.

##### Executive:
1. Supervise the other members of the Executive to ensure that they fulfill their roles in a satisfactory manner and are acting as per the Board’s direction. 
1. Coordinate with the Executive in ensuring that adequate awareness of the Society is generated in the on and off-campus communities.
1. Be responsible for coordinating all necessary awards from the Executive Awards committee, unless otherwise decided.
1. Responsible for coordinating the Executive Retreat and Transition programming.
1. Organize two team development sessions conducted by a third party facilitator once within the first eight months of their term, and once within the last four months of their term.

##### Representation:
1. Represent only the official opinion of the Board when acting on behalf of the Society.
1. Represent the Society within the Faculty of Applied Science.
1. Represent the Society within the Alma Mater Society.
1. Represent the Society on the AMS Students’ Council or appoint a representative to the position in accordance with the governing section of AMS Code. 
Meetings: 
1. Preside over certain meetings of the Society as indicated in EUS Meeting Policy Section 7.4.
1. Sit on all required committees. 
External Relations
1. Be the liaison between the Society and external student groups. 
1. Be the liaison between the Society and other Engineering Student Societies.
1. Work with and represent the Society to external affiliates. 
1. Be responsible for coordinating Society representation at external conferences and competitions. 

#### 5.1.2.3 Assigned Directors
##### The Assigned Directors for President are:
1. Conferences Director

#### 5.1.2.6 Assigned Committees
##### The Assigned Committees for President are:
1. BASC Student Advisory Council
1. EUS Conferences Committee
1. EUS Executive Awards Committee
1. PAF Governance Committee
1. Walter Gage Memorial Fund
1. EUS Executive Committee
1. ESC Governance Committee

### 5.1.3 VP Academic
#### 5.1.3.1 Description
The VP Academic is responsible for all academic, wellness and professional affairs of the Society. The VP Academic is responsible for the proper representation of students at functions relevant to the engineering academic experience and curriculum.

#### 5.1.3.2 Duties
The duties are broken up into the following sections:
##### General:
1. Oversee the academic services offered by the Society. 
1. Work to improve the academic and university experience for members by representing the Society to the University Administration and Faculty. 
Curriculum:
1. Be responsible for the proper representation of students at functions relevant to the engineering academic experience and curriculum.
1. Act as the Society’s first curriculum representative and work in conjunction with the Curriculum Director. 
1. Coordinate with program clubs to ensure program curriculum concerns and issues are addressed. 

##### Senate:
1. Ensure that there is regular communication between the Council and the elected Applied Science Senate Representative.

##### Meetings:
1. Represent the Society on joint Student-Faculty Committees, or ensure a representative is appointed to the position. 
1. Be the primary Curriculum Representative at relevant meetings. 

##### Wellness:
1. Organize health and wellness events for the the society.

##### Graduation:
1. Organize the Iron Ring ceremony including, but not limited to, contacting Camp 5, sizing and ordering rings, and communicating the relevant dates and times to graduates.

#### 5.1.3.3 Assigned Directors
##### The Assigned Directors for VP Academic are:
1. Curriculum Director
1. Graduation Director
1. Health & Wellness Director
1. Tutoring Director

#### 5.1.3.6 Assigned Committees
1. BASC Student Advisory Council
1. EUS Academic Committee
1. EUS Executive Awards Committee
1. EUS Executive Committee

### 5.1.4 VP Administration
#### 5.1.4.1 Description
The VP Administration of the EUS deals with day-to-day and operational aspects of the EUS. The VP Administration is responsible for managing engineering student space, EUS bookings, EUS Council meetings and the EUS Policy Manual.

#### 5.1.4.2 Duties
The duties are broken up into the following sections:

##### General: 
1. Oversee the administrative operations of the Society. 
1. Assume the administrative duties of the President in the President’s absence. 

##### ESC Management:
1. Be responsible for coordinating and administering all matters relating to EUS Student Space including but not limited to:
1. Bookings
1. Maintenance
1. Renovations
1. Neatness
1. Keys & Security Codes
1. Sustainable Practices
1. Track all key disbursements and returns related to the Society.

##### Bookings:
1. Be one of the two booking representatives for the Society. 

##### Policy:
1. Supervise and be familiar with the entire contents of the Policy Manual and Constitution.
1. Coordinate all affairs related to the update and maintenance of the Constitution and Policy Manual.
1. Ensure that all Directors, Managers, and Representatives submit any necessary updates at the end of their terms. 

##### Performance Funding:
1. Distribute Performance Funding in accordance with Performance Funding Policy 10.2.3 in collaboration with the VP Finance.

##### Sustainability:
1. Coordinate the implementation of sustainable practices within the EUS.

#### 5.1.4.3 Assigned Directors
##### The Assigned Directors of VP Administration are:
1. Council Director
1. Bookings Director
1. Facilities Director
1. Sustainability Director

#### 5.1.4.6 Assigned Committees
##### The Assigned Committees for VP Administration are:
1. EUS Executive Awards Committee
1. EUS Executive Committee
1. ESC Governance Committee
1. EUS Events Committee
1. EUS Finance Committee

### 5.1.5 VP Communications
#### 5.1.5.1 Description
The role of the VP Communications is to promote and communicate all activities of the Society. 

#### 5.1.5.2 Duties
The duties are broken up into the following sections:

##### General:
1. Oversee the communication of the activities of the EUS to program clubs, ex-officio clubs, and all EUS members. 
1. Assume the public speaking duties of the President in the President’s absence. 
1. Drive participation in EUS events and activities through direct engagement with students. 

##### e-nEUS:
1. In co-operation with the Faculty of Applied Science, support and oversee the activities of the e-nEUS Editor. 

##### Publications:
1. Manage and negotiate contracts for printing of the Handbuk, Slipstick, and nEUSpaper, and related advertising sales for those publications. 

##### Media:
1. Oversee the collection and production of EUS media including but not limited to photos, video, and graphics.

##### Communications:
1. Regularly gather feedback from program clubs, ex-officio clubs, and EUS members on the activities of the EUS. 
1. Maintain a consistent brand identity and messaging among all EUS publications and communications. 
1. Oversee the social media presence of the EUS, including but not limited to content on the Website, Twitter, and Facebook.

##### Archives:
1. Coordinate the maintenance of all records pertaining to the EUS, both digitally and in hard copy, whenever possible. 

##### Iron Pin:
1. Organize the yearly Iron Pin Ceremony.
1. Coordinate any Iron Pin Reception involved.

#### 5.1.5.3 Assigned Directors
##### The Assigned Directors to VP Communications:
1. Media Director
1. Publications Director
1. Video Director

#### 5.1.5.6 Assigned Committees
##### The Assigned Committees for VP Communications are: 
1. EUS Executive Awards Committee
1. EUS Executive Committee

### 5.1.6 VP Finance
#### 5.1.6.1 Description
The VP Finance is responsible for all financial aspects of the Society. 

#### 5.1.6.2 Duties
The duties are broken up into the following sections: 

##### General:
1. Oversee the financial operations of the Society. 
1. Be over the age of majority at the beginning of the term they are serving to ensure that they are able to legally assume signing authority for the Society. 
1. Arrange any payments of Society debts. 

##### Financial:
1. Ensure that all expenditures made by elected or appointed volunteers and anyone else making purchases on behalf of the Society are fiscally responsible. 
1. Appropriately reimburse approved expenditures made on behalf of the Society. 
1. Keep the Council informed on the Society’s financial status in the form of a written report that is presented to the EUS Council at the end of October, January, and April each year, and as requested by the Council. 

##### Budget:
1. Distribute the Society’s funds in accordance with the budget. 
1. Submit all budgets required by the Vice President Finance of the AMS
1. Assist the other Executive Officers with creating budgets at the beginning of their terms in order to obtain estimates of all proposed expenditures. 
1. Work in conjunction with the incoming VP Finance to prepare the following year’s budget by the end of their term. 

##### Performance Funding:
1. Distribute Performance Funding in accordance with Performance Funding Policy 10.2.3 in collaboration with the VP Admin.

##### Services:
1. Assist and coordinate assigned appointed volunteers to ensure that associated businesses and services are being run as well as possible.
1. This includes, but is not limited to, the ESC Eatery, the ESC Bar, Red Sales, and other businesses. 
1. Be responsible for the organization and implementation of the EUS “Engineer Your Career Fair”.

##### Industry:
1. Be the Society’s liaison officer with companies as they come to campus. 
1. Be responsible for the timely distribution of information on employment opportunities to members of the Society. 
1. Be the liaison between the Society and professional engineering organizations. 
1. Attend, on invitation, meetings and the AGM of APEGBC or send a representative.

#### 5.1.6.3 Assigned Directors
##### The Assigned Directors for VP Finance are:
1. Career Fair Director
1. ESC Eatery Director
1. Professional Development Director
1. Red Sales Director

#### 5.1.6.6 Assigned Committees
##### The Assigned Committees for VP Finance are:
1. EUS Executive Awards Committee
1. EUS Executive Committee
1. PAF Governance Committee
1. PAF Funding Committee
1. EUS Finance Committee

### 5.1.7 VP Spirit
#### 5.1.7.1 Description
The VP Spirit shall strive to increase and promote engineering spirit and community on campus. The VP Spirit oversees traditional EUS events.

#### 5.1.7.2 Duties
The duties are broken up into the following sections:

##### Orientations:
1. Develop a Week E^0 Calendar with consultation with the Executive and the Council. 
1. Organize all Week E^0 events. 
1. Supervise the sale and distribution of tickets for society functions. 

##### E-WEEK:
1. Develop an E-WEEK Calendar with consultation with the Executive, E-WEEK Committee, and the Council. 
1. Organize all E-WEEK events.
1. Create a report with recommendations based on E-WEEK feedback.
1. Supervise the sale and distribution of tickets for society functions. 

##### Alumni:
1. Be the liaison between the Society and community-at-large and alumni. 
1. Maintain close communications with the Faculty of Applied Science Alumni Relations Office.

##### Volunteers:
1. Maintain contact lists of all Appointed Volunteers directly involved with the Society. 
1. Ensure the proper training of EUS general volunteers.

#### 5.1.7.3 Assigned Directors
##### The Assigned Directors for VP Spirit are: 
1. E-WEEK Director
1. Godiva Band Director
1. Volunteer Director
1. Week E^0 Director

#### 5.1.7.6 Assigned Committees
##### The Assigned Committees for VP Spirit are:
1. EUS E-WEEK Committee
1. EUS Executive Awards Committee
1. EUS Executive Committee

### 5.1.8 VP Student Life
#### 5.1.8.1 Description
The VP Student Life is responsible for all events of the Society that are not academic, professional, or traditional in nature. The VP Student Life is responsible for liaising with Program Clubs and other on-campus groups and developing a social engineering community. 

#### 5.1.8.2 Duties
The duties are broken up into the following sections:

##### General: 
1. Enhance student life and environment for the EUS membership.
1. Oversee the events coordinated by the Society. 

##### Charity:
1. Oversee all charity involvement of the Society.

##### Sports:
1. Oversee all sports events of the Society.
1. Organize Engineering involvement in the Faculty Cup.

##### Social:
1. Be well versed in all University policies and procedures related to events on Campus. 
1. Be well versed in all BC Liquor Laws and procedures related to obtaining Liquor Licenses on Campus. 

##### Competitions:
1. Oversee competitive events including but not limited to UBC Engineering Competition and the Genius Bowl.

#### 5.1.8.3 Assigned Directors
##### The Assigned Directors for VP Student Life are:
1. Charity Director
1. Social Director
1. Sports Director
1. UBC Engineering Competition Director

#### 5.1.8.6 Assigned Committees
##### The Assigned Committees for VP Student Life are: 
1. EUS Events Committee
1. EUS Executive Awards Committee
1. EUS Executive Committee
1. EUS Sports Committee

## 5.2 Directors
### 5.2.1 General
#### 5.2.1.1 Description
Directors are the highest non-elected volunteers in the Society. One person should occupy each position but Co-Directors may be appointed for the position with approval from the EUS Board of Governors.
 
#### 5.2.1.2 Duties
##### Each Director shall:
1. Be an Active Member of the Society during the term they are serving.
1. Occupy only one director position within the Society, unless an exception is made by the Board.
1. Serve from when appointed by Board to April 30.
1. Receive direction from the Executive Officer to which they are assigned.
1. Be expected to dedicate 10 or more hours a week during the academic term.
1. Uphold the Constitution and policies of the Society.
1. Ensure the Managers and Representatives for which they are responsible are properly trained.
1. Oversee the Managers and Representatives for which they are assigned and ensure that they fulfill their roles in a satisfactory manner.
1. Regularly report their activities and the activities of their assigned volunteers to the Executive Officer to which they are assigned.
1. Absorb, re-delegate, or otherwise be responsible where necessary for any and all duties not satisfactorily carried out by those Managers and Representatives assigned to them.
1. In the case where a Manager or Representative is not filled, the Director shall absorb, re-delegate, or otherwise be responsible for all duties of that position.
1. Promote the Society to both the EUS Members and the external community.
1. Assign an assistant, upon the advice of their respective executive,  to take care of some of the duties assigned to their portfolio as necessary.
1. Coordinate with executives and other directors as needed to ensure the logistics of their event or service are carried out.
1. Train their replacements at the end of their term and provide them with any relevant files, reports, introductions, and paperwork needed for them to do their roles effectively.
1. Have other duties as assigned.

## 5.3 Managers
### 5.3.1 General
#### 5.3.1.1 Description
Managers are the mid-level volunteers in the Society. One person should occupy each position but Co-Managers may be appointed for the position with approval from the EUS Board of Governors.
 
#### 5.3.1.2 Duties
##### Each Manager shall:
1. Be an Active Member of the Society during the term they are serving.
1. Occupy only one manager position within the Society, unless an exception is made by the Board.
1. Serve from when appointed by Board to April 30.
1. Receive direction from the Executive Officer and Director to which they are assigned.
1. Be expected to dedicate approximately 5 to 10 hours a week during the academic term.
1. Uphold the Constitution and policies of the Society.
1. Ensure the Representatives for which they are responsible are properly trained.
1. Oversee the Representatives for which they are assigned and ensure that they fulfill their roles in a satisfactory manner.
1. Regularly report their activities and the activities of their assigned volunteers to the Director and Executive Officer to which they are assigned.
1. Absorb, re-delegate, or otherwise be responsible where necessary for any and all duties not satisfactorily carried out by those Representatives assigned to them.
1. In the case where a Representative is not filled, the Manager shall absorb, re-delegate, or otherwise be responsible for all duties of that position.
1. Coordinate with executives, directors, and other managers as needed to ensure the logistics of their event or service are carried out.
1. Promote the Society to both the EUS Members and the community-at-large.
1. Train their replacements at the end of their term and provide them with any relevant files, reports, introductions, and paperwork needed for them to do their roles effectively.
1. Have other duties as assigned.

## 5.4 Representatives
### 5.4.1 General
#### 5.4.1.1 Description
Representatives are the entry-level volunteers in the Society. Multiple individuals may be appointed into each of these roles upon the approval of the Executive.
 
#### 5.4.1.2 Duties
##### Each Representative shall:
1. Be an Active Member of the Society during the term they are serving.
1. Serve from when appointed by the Executive to April 30.
1. Receive direction from the Executive Officer, Director, and Manager to which they are assigned.
1. Be expected to dedicate approximately 5 hours a week or as necessary during the academic term.
1. Uphold the Constitution and Policies of the Society.
1. Regularly report their activities to the Manager, Director, and Executive Officer to which they are assigned.
1. Promote the Society to both the EUS Members and the community-at-large.
1. Train their replacements at the end of their term and provide them with any relevant files, reports, introductions, and paperwork needed for them to do their roles effectively.
1. Have other duties as assigned.

## 5.5 Applied Science Student Senator
### 5.5.1 Description
The Applied Science Student Senator represents UBC APSC students to the UBC-V Senate - the highest academic body of the University. The EUS has no authority over this position; the duties of the Applied Science Student Senator are recommendations by the EUS only. 

### 5.5.2 Duties
##### The Applied Science Student Senator should:
1. Attend Grand Council
1. Attend EUS Council
1. Attend BASC Student Advisory Council
1. Stay informed of EUS activities and initiatives
1. Communicate broad, engineering pertinent, and academic policy of the UBC Senate matters to the EUS VP Academic

## 5.6 EUS AMS Representatives
### 5.6.1 Description
EUS AMS Representatives are designates for Engineering on AMS Council. The general engineering undergraduate student membership elects them as outlined in Elections Policy Section 9.2. Their duties for the AMS are outlined in AMS Code and are as follows. 

### 5.6.2 Description
##### EUS AMS Representatives shall:
1. Attend all AMS Council meetings during the school year (September through March) or provide a proxy. Even if they provide a proxy, Constituency representatives will lose their seats on Council if they miss five regular meetings of Council during the school year.
1. Seek to be appointed to at least one AMS standing committee.
1. Attend EUS Council Meetings
1. Stay informed of EUS activities and initiatives.

