---
title: "13. Appendix A"
weight: 13
draft: false
---

## 13.1 Background
### 13.1.1 Fostering Leadership
Throughout the years, undergraduate engineers have exhibited outstanding leadership and initiative to help their fellow students. Often, they receive very little reward or recognition for their efforts. In order to foster growth, leadership, initiative, and innovation in the engineering community, the Engineering Undergraduate Society (EUS) feels it is important for the community to acknowledge and give back to those individuals who go above and beyond expectations and positively impact UBC engineering.

### 13.1.2 Source
In November 2008, the EUS Board of Directors voted unanimously to create an endowment fund whose interest will fund awards rewarding members' leadership, innovation, initiative, and service to the community. The Board added budgetary surpluses until the fund was able to generate $3,000 in interest a year. The fund, now housed with UBC Awards, generates approximately $3,000 for award dispersal per year.

### 13.1.3 Description
Every year the EUS shall distribute at least four Community Contribution Awards, provided there are sufficient qualified applicants. Of the awards, two shall be Junior Awards ($500 value each) for student who have completed two to five academic terms in the pursuit of their B.A.Sc.. Two shall be Senior Awards ($1,000 value each) for student who have completed over five academic terms in the pursuit of their B.A.Sc.. If there is over $3,500 in the spend account, another $500 award shall be awarded to an extraordinary Junior or Senior Applicant who was not selected for one of the four awards described above. 

Applications shall close in September and recipients recommended to the UBC Awards Office by October 31st.

### 13.1.4 Eligibility

1. The applicant must be an active member of the UBC (Vancouver) Engineering Undergraduate Society and full-time student at the time of the award disbursal:
    1. Active members of the Engineering Undergraduate Society are defined as "Undergraduate Engineering students who have paid their EUS student fees," according to the EUS Constitution.
    1. Full-time student is defined by the province of BC: a student must be enrolled in 60% of his/her program’s full-time course load.
1. The applicant must be in Good Standing at the time of the award disbursal:
    1. Good Standing is defined by the UBC Calendar: students must achieve a credit-weighted average of at least 55% and pass at least 65% of all courses taken since the last academic performance.
1. An academic term shall be defined as a winter regularly scheduled session term during which the student was a full time student at UBC, excluding co-op and exchange.
1. The applicant must be applying for the appropriate category of sponsorship:
    1. The $500 Junior Awards are only eligible to those who have completed two to five academic terms in pursuit of their B.A.Sc. by the application period.
    1. The $1,000 Senior Awards are only eligible to those who have completed over five academic terms in pursuit of their B.A.Sc. by the application period.
    1. If there is over $3,500 in the spend account, both Junior and Senior Award applicants are eligible for the additional $500 award.
1. The applicant must have fully completed and submitted an application before the designated due date.
1. Co-op students are still eligible for the full value of the award.
1. The applicant must not have received the same category of award previously.
1. The applicant must not be a member of the following:
    1. Selection Committee
    1. Incoming Board of Directors

### 13.1.5 Selection Criteria
1. The applicant will be selected based on demonstrated: 
    1. Leadership
    1. Innovation
    1. Personal Initiative
    1. Service to the UBC engineering student community
1. If possible, the selected recipients must collectively represent the diversity of the engineering community.
1. Please note that grades are not considered in selecting recipients for these awards, so long as the applicant is in good standing with the university.
