---
title: "6. Clubs and Affiliate"
weight: 6
draft: false
---

## 6.1. Alma Mater Society

The Alma Mater Society \(AMS\) is the Student Society of UBC and the parent organization of the EUS. All EUS Members are automatically members of the AMS and pay AMS fees.

The EUS receives one seat on the AMS Council and then an extra seat for every 3000 students as outlined in AMS Code.

## 6.2. EUS Ex-Officio Clubs

### 6.2.1. Definition

Ex-Officio Clubs are a recognized group of Engineering students other than programDepartment Clubs.

### 6.2.2. Becoming an Ex-Officio Club

An organization must:

1. Have a mission statement related to the Engineering Profession in order to be considered for Ex-Officio Club status.

2. Be open to students in all Engineering Programs at UBC to be considered for Ex-Officio Club status. The proposed ex-officio club may primarily run services and events targeted at a particular engineering program, so long as membership is still open to students in all programs.

3. Make a presentation to the Council explaining their organization and why they would like to receive Ex-Officio Club status.  A motion must then be moved and passed by the Board to accept the organization as an Ex-Officio Club.  Since this results in a change to the Policy Manual, a 2/3rd in-favour vote must occur.

   1. EUS Council will also give the Ex-Officio an Assigned Committee at this point. The responsibility of an Assigned Committee is explained below.

### 6.2.3. Powers and Duties

1. Ex-Officio Clubs are welcome to give an update at Informational and Council meetings as defined in EUS Meetings Section 7.4.

2. Ex-Officio Clubs shall be included on the Council mailing list.

1. Ex-Officio Clubs shall receive the same privileges as Program Clubs in terms of booking of facilities and use of services provided by EUS volunteers.

2. Ex-Officio Clubs shall send at least one representative every meeting of their Assigned EUS Committee.

3. Ex-Officio Clubs may also choose to send representatives to the following EUS Committees:

   1. EUS Events Committee

   2. EUS E-WEEK Committee

   3. EUS Sports Committee

### 6.2.4. Removal of Ex-Officio Club Status

An Ex-Officio Club may lose its Ex-Officio Club status for any of the following reasons:

1. Resignation of status

2. Failure to send a representative to more than two consecutive Assigned Committee Meetings.

   1. The Chair will issue a written warning to the President of the Ex-Officio Club following the first missed meeting and at least one week before the next Assigned Committee meeting.

1. By request through a petition signed by three-quarters of the Board of Governors or one-tenth of the active membership.

### 6.2.5. Recognized Clubs

1. Alpha Omega Epsilon Professional Engineering Sorority \(ΑΩΕ or AOE\)

   1. Assigned Committee: EUS Events Committee

2. Canadian Institute of Mining \(CIM\)

   1. Assigned Committee:

3. Engineering Design Teams Council \(EDTC\)

   1. Assigned Committee:

4. Engineers Without Borders \(EWB\)

   1. Assigned Committee: EUS Events Committee

5. Gears and Queers \(G&Q\)

   1. Assigned Committee: EUS Events Committee

6. Institute of Electrical and Electronics Engineers \(IEEE\)

   1. Assigned Committee:

7. Sigma Phi Delta Professional Engineering Fraternity \(ΣΦΔ or SPD\)

   1. Assigned Committee: EUS Events Committee

8. Women in Engineering \(WiE\)

   1. Assigned Committee: EUS Events Committee

9. Engineers for a Sustainable World \(ESW\)

   1. Assigned Committee:

## 6.3. EUS First Year Council

### 6.3.1. Description

The EUS First Year Council \(FYC\) is the committee of elected and hired first year representatives of the Engineering Undergraduate Society. The FYC is a subsidiary of the EUS, at no point should their actions conflict the values outlined by the EUS.

### 6.3.2. Composition

The EUS FYC will be composed of the following elected and hired voting positions; each position shall hold one vote:

Elected:

1. FY President

2. FY Vice President

3. FY Academic Representative

4. FY Social Coordinator

5. FY Sports and Charity Representative\(s\) \(maximum 2\)

6. FY Vantage Representative

Hired:

1. FY Secretary

2. FY Treasurer

3. FY Publicity Representative\(s\) \(maximum 2\)

4. FYC Extraordinary Members \(maximum 3\)

EUS FYC shall not exceed 14 members. In this case, there would be the maximum number of council members according to each position's description \(11\), plus a maximum of 3 Extraordinary Members.

Hired members shall be ratified by majority vote at a FYC meeting following an interview consisting of at least two elected councilors. Interviews and hiring must be completed within three weeks of the announcement of FYC election results, with the exception of Extraordinary Members whose membership shall be considered throughout the year.

### 6.3.3. Branding

The EUS FYC shall:

1. Approve all logos through the EUS VP Communications.

2. Have the nickname and occasionally be referred to as “PP Council”.

3. Receive “p” patches for each member of FYC at the end of their term.

### 6.3.4. Duties

Each Elected Member of the First Year Council shall:

1. Be an Active Member of the Society during the term they are serving.

2. Occupy only one position within the FYC.

3. Serve from ratification of FYC Election Results until ratification of the following FYC Election Results for the new First Year Class as dictated by the EUS Elections Policy Section 9.2.22.

4. Be expected to dedicate between 5-10 hours a week during the academic term to the FYC.

5. Report their actions and decisions to FYC.

Each Hired Member of the First Year Council shall:

1. Be an active member of the society during the term that they are serving.

2. Occupy only one position within the FYC.

3. Serve from the time that they are hired until ratification of the following FYC Election Results for the new First Year Class as dictated by EUS Elections Policy Section 9.2.22.

4. Be expected to dedicate between 5-10 hours a week during the academic term to the FYC

5. Be hired by majority vote of the FYC following an interview involving at least two elected FY councillors.

The First Year Council shall:

1. Uphold the policies of the FYC.

2. Promote the Society to the First Year Class, other EUS Members and the community-at-large.

3. Train their replacements and provide them with a transition document containing any relevant files, reports, introductions, and paperwork needed for them to do their roles effectively

4. Hold meetings at a minimum of once every two weeks. These meetings shall be open to all members of the First Year Class, andthe EUS Executive, and a proxy appointed by the EUS executive. The meeting time and location should be publicly available.

### 6.3.5. First Year President

#### 6.3.5.1. Description

The President shall serve as the primary representative and voice of the

FYC. They will be responsible for articulating the mission, vision, direction, and opinions of the First Year Class. The main job of the President is to manage and support the FYC. The second job of the President is to be the liaison and voice of the FYC to the EUS Council.




#### 6.3.5.2. Duties

The President shall:

General:

1. Be occupied by only one Active Member of the First Year Class.

2. Hold one vote on the FYC.

3. Coordinate the affairs of the FYC.

4. Be the official spokesperson of the FYC on all occasions.

5. Adhere to and be familiar with the FYC Policy as found in the EUS Policy Manual.

6. Absorb, delegate, or otherwise take responsibility for any and all duties that are not adequately performed by other Representatives or do not fall within any other Representatives’ portfolios.

7. Attend or assign a proxy to attend all Assigned EUS Committees.

FYC:

1. Supervise the other members of the FYC to ensure that they fulfill their roles in a satisfactory manner.

2. Coordinate with the FYC to ensure adequate awareness of the FYC by all members of the First Year Class.

Representation:

1. Represent only the official opinion of the FYC when acting on behalf of the FYC.

2. Represent the FYC to the EUS Council.

3. Report on activities of the FYC to EUS Council.

Meetings:

1. Preside over meetings of the FYC unless otherwise decided by the FYC.

External Relations:

1. Be the liaison between the EUS FYC and the AMS First Year Committee.

#### 6.3.5.3. Assigned Committees

The Assigned Committees for President are:

1. BASC Student Advisory Council

2. EUS Council

### 6.3.6. First Year Vice President

#### 6.3.6.1. Description

The role of the FYC Vice President is to support the President, take on special projects related to the FYC.

#### 6.3.6.2. Duties

The Vice President shall:

General:

1. Be occupied by only one Active Member of the First Year Class.

2. Hold one vote on the FYC.

3. Support the President in FYC matters.

4. Assume the operating duties of the President in the President’s absence.

5. Assume the public speaking duties of the President in the President's absence.

6. Take on special projects as necessary for FYC.

7. Attend or assign a proxy to attend all Assigned EUS Committees.

8. Co-ordinate the hiring process of all hired positions of the FYC

E-WEEK:

1. Be responsible for the First Year Council E-WEEK Team.

2. Coordinate FY involvement in E-WEEK.

3. Be responsible for the creation or modification of the FY Chariot and Ball Model.

#### 6.3.6.3. Assigned Committees

The Assigned Committees for the Vice President are:

1. EUS E-WEEK Committee

### 6.3.7. First Year Secretary

#### 6.3.7.1. Description

The FY Secretary is responsible for the internal and operation aspects of the FYC.

#### 6.3.7.2. Duties

The FY Secretary shall:

General:

1. Be occupied by only one Active Member of the First Year Class.

2. Be a non-voting member of the FYC.

3. Oversee the administrative operations of the FYC.

Bookings:

1. Be responsible for coordinating with the EUS VP Administration for FYC bookings.

Policy:

1. Coordinate all affairs related to suggestions,updates and proposed changes to the FYC Policy in the EUS Policy Manual. Note that any changes to FYC Policy must be passed by EUS Council.

Meetings:

1. Record minutes of all FYC meetings.

2. Submit meeting minutes to the EUS VP Administration.

3. Schedule and book all FYC meetings.

### 6.3.8. First Year Treasurer

#### 6.3.8.1. Description

The FY Treasurer is responsible for all financial aspects of the FYC.

#### 6.3.8.2. Duties

The FY Treasurer shall:

General:

1. Be occupied by only one Active Member of the First Year Class.

2. Be a non-voting member of the FYC.

3. Oversee the financial operations of the FYC.

Financial:

1. Ensure that all expenditures made by elected or appointed volunteers and anyone else making purchases on behalf of the FYC are fiscally responsible.

2. Copy then submit any receipts and debts to the EUS VP Finance for reimbursement.

3. Keep the FYC informed on its financial status.

Budget:

1. Create the FYC Budget.

2. Distribute the Society’s funds in accordance with the budget.

3. Submit all budgets, as requested, by the EUS VP Finance.

### 6.3.9. First Year Academic Representative

#### 6.3.9.1. Description

The FY Academic Representative is responsible for all actions and initiatives of the FYC related to the first year academic experience.

#### 6.3.9.2. Duties

The FY Academic Representative shall:

General:

1. Be occupied by only one Active Member of the First Year Class.

2. Hold one vote on the FYC.

3. Work with the EUS VP Academic to improve the academic experience for the First Year Class.

4. Maintain an active line of communication between students and faculty regarding academic feedback or concerns with the structure, execution, or instructors of a course in curriculum.

5. Attend or assign a proxy to attend all Assigned Committees.

Curriculum:

1. Be responsible for the proper representation of the First Year Class at functions relevant to the engineering academic experience and curriculum.

Grand Council

1. Represent the FYC along with the FY President at Grand Council meetings.

#### 6.3.9.3. Assigned Committees

The Assigned Committees for Academic Representative are:

1. EUS Academic Committee

### 6.3.10. First Year Social Coordinator

#### 6.3.10.1. Description

The FY Social Coordinator is responsible for the social, non-academic events of the FYC.

#### 6.3.10.2. Duties

The FY Social Coordinator shall:

General:

1. Be occupied by only one Active Member of the First Year Class.

2. Hold one vote on the FYC.

3. Enhance the student life and social environment for the First Year Class.

4. Run social events coordinated by the FYC.

5. Attend or assign a proxy to attend all Assigned Committees.

6. Be a major contributor to the spirit and excitement of the FYC.

#### 6.3.10.3. Assigned Committees

The Assigned Committees for Social Coordinator are:

1. EUS Events Committee

### 6.3.11. First Year Sports and Charity Representative\(s\)

#### 6.3.11.1. Description

The FY Sports and Charity Representative\(s\) are responsible for the coordination of sports and charity events for FYC.

#### 6.3.11.2. Duties

The FY Sports and Charity Representative\(s\) shall:

General:

1. Be occupied by up to two Active Members of the First Year Class.

2. Hold one vote on the FYC.

3. Oversee the sports and charity events and services of the FYC.

Sports:

1. Be responsible for coordinating FY teams for UBC Recreation Intramurals leagues and events.

2. Be responsible for coordinating additional sport events for FY

Charity:

1. Be responsible for coordinating FY involvement in EUS Charity events such as EUS Blood Drive, Movember, and Pi Week.

#### 6.3.11.3. Assigned Committees

The Assigned Committees for Sports and Charity Representative are:

1. EUS Sports Committee

### 6.3.12. First Year Publicity Representative\(s\)

#### 6.3.12.1. Description

The FY Publicity Representative\(s\) role is to promote and communicate all activities of the FYC.

#### 6.3.12.2. Duties

The FY Publicity Representative\(s\) shall:

General:

1. Be occupied by up to two Active Members of the First Year Class.

2. Be a non-voting member of the FYC.

3. Oversee the communication of the activities of the FYC to the First Year Class.

4. Drive participation in FYC and EUS events and activities through direct engagement with students.

e-nEUS:

1. Be responsible for submitting all FYC activities to the e-nEUS.

Publications:

1. Be responsible for compiling and creating all material related to the First Year Class for the associated section of the Slipstick Yearbook.

Communications:

1. Maintain a consistent brand identity and messaging among for FYC.

2. Oversee the social media presence of the FYC, including but not limited to content on the Website, Twitter, and Facebook.

### 6.3.13. First Year Vantage Representative

#### 6.3.13.1. Description

The FY Vantage Representative role is to advocate for Vantage One Engineering Students to the FYC and the EUS.

#### 6.3.13.2. Duties

The FY Vantage Representative shall:

General:

1. Be occupied by an Active Members of the First Year Class.

2. Hold one vote on the FYC.

3. Oversee the communication to Vantage One Engineering Students of the activities of the FYC to the First Year Class.

Social:

1. Drive participation of Vantage One Engineering Students in FYC and EUS events and activities through direct engagement with students.

Academics:

1. Represent the academic Interests of Vantage One Engineering Students to the FYC and to the EUS VP Academic.

Communication:

1. Be responsible for advertising EUS and FYC events to Vantage One Engineering Students.

### 6.3.14. First Year Council Extraordinary Members

#### 6.3.14.1. Description

FYC Extraordinary Members will be appointed on the basis of dedication and service to the FYC throughout the year.

#### 6.3.14.2. Duties

Each FY Extraordinary Members shall:

General:

1. Be an Active Member of the First Year Class.

2. Be a non-voting member of the FYC.

3. Receive responsibilities as designated.

## 6.4. EUS Program Clubs

### 6.4.1. General

1. A program club shall be a fully recognized organization of the EUS members within a specific engineering department or program.

2. Each club will receive funding from the EUS as set out in the Clubs Funding Policy Section 10.2.

3. All clubs shall report to the EUS as outlined in the Clubs Funding Policy Section 10.2.

### 6.4.2. Recognized Clubs:

1. Chemical and Biological Engineering Undergraduate Club \(CHBE\)

2. Civil Engineering Club \(CIVL\)

3. Electrical and Computer Engineering Student Society \(ECESS\)

4. Environmental Engineering Student Association \(ENVE\)

5. The UBC Engineering Physics Student Association \(FIZZ\)

6. Geological Engineering Club \(GEOROX\)

7. Integrated Engineering Club of UBC \(IGEN\)

8. UBC Materials Engineering Undergraduate Club \(MTRL\)

9. Mechanical Engineering Club \(MECH\)

10. UBC Mining Engineering Undergraduate Club \(MINE\)
