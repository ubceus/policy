---
title: "9. Appointment Policies"
weight: 9
draft: false
---

## 9.1 Appointment Policy
### 9.1.1 Description
This policy defines the formal procedures for eligibility, application, selection and removal of all Appointed Volunteers. This policy has no bearing on the election process for Executive Officers or other Elected Positions. 

Appointed Volunteers are all Directors, Managers and Representatives. The suggested timeline for hiring shall be:
1. Directors shall be hired during the month of April
1. Managers shall be hired during the month of May
1. Representatives shall be hired during the month of September
1. Exceptions can be made to this timeline, as the Executive deems necessary. 

### 9.1.2 Eligibility
1. Applicants must be Active Members of the Society during the term of their appointment.
1. Applicants must not hold an Executive position during the term, which they are applying for. 
1. Applicants can only hold a maximum of one Director position and one Representative position, one Manager position and one Representative position, or multiple Representative positions.

### 9.1.3 Application
1. Applicants are required to fill out and submit an application form.
1. The application form shall be open for a minimum of two weeks. 
1. These forms shall be made available both online and in hard copy in the Society’s Student Space.

### 9.1.4 Selection
1. All Appointed Volunteers shall be selected through an interview process.
1. The interview shall be conducted by the executive to whom they will be reporting.
1. The interview shall be conducted by two members of the Board of Governors. Executive officers-elect can be considered as members of the Board of Governors for this purpose.
1. Preferences will be given to  volunteers who do not hold other Appointed Volunteer positions. 
1. Selections for Directors and Managers shall be made, approved by the Board and the results communicated to applicants.
1. Selections for Representatives shall be made, approved by the Executive and the results communicated to applicants. 
1. Any complaints regarding the selection process must be made to the VP Administration no more than 24-hours following the communications results.
1. The final results will be made public as soon as any complaints are resolved and will be announced at the Board meeting following and posted on the website.

### 9.1.5 Removal
The removal policy for an Appointed Volunteer can be for any of the following reasons:
1. Resignation
1. Unsatisfactory performance by resolution of the EUS Executive Committee.
    1. Any appeals can be made to the EUS Board of Governors.

## 9.2 EUS Elections Policy  
### 9.2.1 Description
This policy defines the formal procedures for all EUS Elections. The Elections Policy as a whole applies to each type of EUS Election.

### 9.2.2 Types of EUS Elections
#### 9.2.2.1 EUS General Elections: 
1. EUS General Elections include all Executive Officers of the Society, EUS AMS Representatives and other Elected Positions as determined by the Board.
1. EUS General Elections are to be held once per academic year in late February to mid-March. 
1. EUS General Elections will follow the General Election Timeline, as outlined in Election policy 9.2.4.1.
1. Specific dates are to be set by the Elections Administrator with special consideration to avoid conflicting with AMS Election dates.

#### 9.2.2.2 EUS First Year Elections:
1. EUS First Year Elections include all First Year Council Positions as outlined in First Year Council Policy Section 6.3.
1. EUS First Year Elections are to be held once per academic year in September.
1. EUS First Year Elections will follow the Special Election Timeline, outlined in Election policy 9.2.4.2.
1. Specific dates are to be set by the Elections Administrator with special consideration to avoid conflicting with Week E^0 dates.

#### 9.2.2.3 EUS By-Elections:
1. In the event that an Executive Officer of the Society or EUS AMS Representative resigns or is removed from their position or a tie occurs in the EUS General Elections, a by-election shall be organized within a week by the Elections Administrator.
1. Until the by-election results have been ratified, EUS Council may choose to appoint an interim to the unfilled position
1. EUS By-Elections will follow the Special Election Timeline, outlined in Election policy 9.2.4.2.
1. Specific dates are to be set by the Elections Administrator with special consideration to avoid conflicting with other events.

### 9.2.3 General 
1. Elections for all positions shall take place concurrently.
1. The Elections Administrator shall follow responsibilities as outlined in Elections Section 9.2.

### 9.2.4 Timeline
#### 9.2.4.1 General Election Timeline
1. Voting shall take place over one week.  Voting shall be carried out for a minimum of 3 days during the week.
1. The campaign period shall be two weeks. The campaign period shall start one week before voting opens and close with the voting period.
1. Nominations shall be open for no less than one week. Nominations shall close at 4:00pm the Friday one week before the start of the campaign period. 

#### 9.2.4.2 Special Election Timeline
1. Voting shall take place over a minimum of two days. 
1. The campaign period shall be a minimum of four days. The campaign period shall start a minimum of two days before voting opens and close with the voting period.
1. Nominations shall be open for no less than one week. Nominations shall close a minimum of eight hours before the start of the campaign period. 

### 9.2.5 Removal of Elected Officials
The removal policy for an Executive Officer can be for any of the following reasons:

1. Resignation
1. Absence from three consecutive Council Meetings
1. Absence from two consecutive Executive Meetings, without justification deemed valid by the Board of Governors
1. Unsatisfactory performance according to a petition signed by three-quarters of the Board of Governors or one-tenth of the active membership.

### 9.2.6 Nominations
##### General:
1. Nominations shall be submitted on official forms provided by the Elections Administrator.
1. Nominees will be required to supply their UBC Student Number, full name, and contact information consisting of a phone number and email address.
1. Nominees will be required to sign their nomination form, which shall be taken as an indication of their willingness to stand for election, their understanding of their duties as defined in the Policy Manual and Constitution, and confirmation of their eligibility according to the Policy Manual and Constitution.
1. Nomination forms must be signed by at least ten Active Members of the Society who are not current Board Members.
1. The Elections Administrator shall be responsible for checking nominations to ensure that each form is complete, all provided information is correct, and the student number corresponds to an Engineering Student.
1. Completed nomination forms may be submitted in person to the Elections Administrator, scanned and emailed to the Elections Administrator or placed in a predetermined receptacle as indicated by the Elections Administrator.

##### Withdrawal of Nominations:
1. Candidates intending to withdraw prior to the election are required to notify the Elections Administrator in writing.
1. If a candidate withdraws after the ballots have been finalized, the withdrawal and the Candidate’s name on the ballot shall not invalidate the election.
1. Votes cast for a candidate who has withdrawn shall not be counted.

### 9.2.7 Campaign Regulations
#### 9.2.7.1 General: 
1. Campaigning shall take place only during the campaign period as specified Timeline Section 9.2.4. This includes other persons or organizations campaigning on the behalf of candidates.
1. Any campaign irregularities are to be reported to the Elections Administrator.
1. Candidates may not spread libel or slander other candidates.
1. Candidates are encouraged to use multiple methods of campaigning. 
1. Candidates shall not run in slates, real or apparent, or share expenses for campaign materials.  A slate shall mean a group of candidates who run for elected office on a similar platform for mutual advantage.
1. Electoral officers are not to offer opinions on the election, or the candidates. If electoral officers are asked questions about the candidates, they shall direct the voter to candidate materials, or the elections webpage.

#### 9.2.7.2 Campaign Materials:
1. No materials may contain any libel regarding other candidates.
1. The Elections Administrator must approve all materials before distributed.  The Elections Administrator will take no longer than 24 hours from the time of submission to make a decision on the material, and if rejecting the material, will specify exact Elections Policy sections the material is violating. 
1. No materials may contain material that is offensive due to its sexist, racist, pornographic, homophobic, or otherwise inappropriate content, as described in the EUS Publications Policy.
1. Although no reimbursements will be made for campaign expenses, each candidate is restricted to spending $150 at fair market value on campaign materials.
1. Materials shall not contain the EUS logo, the UBC Engineering logo, the UBC Alma Mater Society logo or The University of British Columbia logo, or any branding associated with those entities.
1. Candidates may wear engineering paraphernalia in their campaign photos so long as none contains any official branding associated with these entities.  
1. All non-text materials shall contain the EUS Elections logo, which shall be provided to candidates by the Elections Administrator one week before the start of the Campaign period.

#### 9.2.7.3 EUS Publicity:
1. The Elections Administrator shall ensure that the Society’s website contains all relevant elections information, including digital copies of all candidates’ statements and photographs.
1. Each candidate is required to submit a 250 word maximum statement and photograph to the Elections Administrator at least one week prior to voting for the EUS Website.
1. Each candidate is required to submit a 100 word maximum statement and photography to the Elections Administrator at least one week prior to voting for the Ballot.
1. Once submitted updates or changes will not be made to the statement or photo. 

#### 9.2.7.4 Posters:
1. Candidates must submit a digital version of their poster in PDF format to the Elections Administrator at least three business days prior to voting.  
1. Candidates are responsible for their own poster distribution and are encouraged to keep poster distribution minimal. Candidates must adhere to the UBC Postering Policy.
1. Candidates are highly encouraged to remove all posters put up by them or their associates within two business days following the end of the election.

#### 9.2.7.5 Online Campaigning:
1. Candidates are not permitted to use pre-existing lists for the purpose of campaigning.  Lists shall be defined as:
    1. Mailing lists, including email
    1. Pre-existing social media groups or pages
    1. Similar concepts as determined by the Elections Administrator
    1. An exception to this is personal social media pages which may be used to promote voting but only during the campaigning period. 
1. Candidates are not permitted to use any form of “spamming” – unsolicited bulk email or other direct online communication.
1. If endorsed by an organization, the organization may send out campaign materials over their existing mediums. 

#### 9.2.7.6 Endorsements: 
1. Current Executive Officers cannot endorse candidates in EUS General, First-Year or By-Elections. 
1. Outgoing Executive Officers who are not returning to the Board can endorse candidates in Program Club Elections.
1. Incoming Executive Officers Elects or return Executive Officers cannot endorse candidates in Program Club Elections. 
1. Candidates may receive endorsements from individuals or from groups.
1. Candidates may not use the mailing lists or messaging mediums of these groups for the purposes of campaigning.

#### 9.2.7.7 Warnings and Disqualification:
1. Failure to adhere to the regulations specified in this policy may result in disqualification from the election.
1. The Elections Administrator is the only person authorized to determine repercussions of breaking these regulations.
1. Candidates found to be violating any of the campaign or poster regulations shall be issued a formal written warning.
1. If a candidate is still found to be in violation after two days, they will be disqualified from the election unless otherwise decided by the Elections Administrator.

### 9.2.8 Voting Procedure
1. Elections shall be preferentially carried out online a secure, regulated system to ensure only eligible students can cast a vote. 
1. There shall be at least one polling station with a computer available if voting is available online, or at least three physical ballot boxes in separate Engineering locations if voting is not available online.
1. For each position, a “Reopen Nominations” option shall be made available, regardless of the number of candidates running.  Should this option receive the most votes in a given race, a by-election shall be held for that position as soon as possible.

### 9.2.9 Election Results
1. Election results will be announced the day voting closes at the relevant event as well as online through the website and social media. 
1. Candidates shall be given three business days to contest any results.
1. The winner of the election for each position is the candidate with the most votes.  This candidate must win by a margin that is greater than the number of spoiled ballots.
1. In the event of a tie a by-election shall be held for that position.

### 9.2.10 Appeals:
1. Any allegations of irregularities must be submitted to the Elections Administrator in writing within 24-hours of notification of the results.
1. Upon receipt of an allegation submitted, a meeting of the Board shall be called and the results of the election shall be unofficial pending the decision of the Board as to whether the election shall be declared void.
1. The decision of the Board shall be final and shall be submitted to the members of the EUS in the next issue of the e-nEUS.
1. All appeals, and replies to said appeals, will be made available to the public.

### 9.2.11 Election Events
#### 9.2.11.1 All Candidates Meeting:
1. All candidates shall attend a meeting after the close of nominations but before the start of campaigning for the purpose of explaining rules, regulations, scheduling and any other details regarding the elections.
1. Candidates must arrange to meet with the Elections Administrator in the event that they are unable to attend the all candidates meeting as soon as possible after the close of nominations.

#### 9.2.11.2 Candidate Debates:
1. The forum shall be carried out in a neutral location, preferably a large classroom or small lecture hall.
1. Rules:
    1. All candidates must adhere to UBC’s Respectful Environment policy.
    1. All candidates must be given equal opportunity to speak and answer their questions.
    1. In the event a candidate cannot make the meeting, they may designate a proxy to answer questions.  The Elections Administrator must pre-approve this proxy.  
    1. The Elections Administrator has the right stop the forum if candidates or audience members do not adhere to UBC’s Respectful Environment Policy.

#### 9.2.11.3 Other Events:
1. The Elections Administrator shall organize other events as needed to promote voter turnout.

### 9.2.12 Election Administrator Disputes 
If there are any disputes with the Elections Administrator’s rulings or behaviour, a candidate may bring up concerns, in writing, to the EUS Board of Governors.  
In the event that the Elections Administrator is deemed unfit or unavailable to do their job, the Executive may designate a successor that must be approved by the Board.  This approval may come in email format.  

## 9.3 Referenda
### 9.3.1 Description
This policy shall govern all Society referenda.

### 9.3.2 Procedure
1. A referendum or referenda may be called by the President by:
    1. A resolution of the Board or;
    1. A written petition containing the names, student numbers and signatures of at least five percent (5%) of the EUS.
1. The text of the referendum or referenda shall be written in such a way that it must be unbiased and answerable unequivocally as yes or no. Such text shall be subject to approval or amendment by the Board. 
1. The Board has the power to remove referendum questions if they go against UBC’s Non-Academic Misconduct Policy with a super majority (⅔).
1. The Board must approve referendum questions at least three weeks prior to voting opening. Voting shall open no more than a month after approval by the Board.
1. It is recommended that referenda occur during EUS General Elections.

### 9.3.3 Voting
1. A referendum or referenda shall only be held during the academic year inclusive with the exception of over holidays, reading week and examination periods. 
1. The voting period for the referendum will be no less than 3 days. 
1. Referenda will be preferentially carried out on WebVote, or a similar system. If a paper ballot is necessary or the only available option, ballot boxes shall be staffed for at least 12 hours over the 3 day minimum period, and shall be in at least 3 locations frequented by Engineering Students.
1. Unofficial results will be made public immediately.
1. The Board must approve all referendum results. 

### 9.3.4 Results
A referendum or referenda shall be acted upon by the Board where:
1. A majority of the votes cast support the referendum or referenda and;
1. The number of votes cast is equal or greater to ten percent (10%) of the members of the EUS in good standing. 
1. Unless the referendum is to accept changes to the Constitution, in which case a 2/3rds approval must be met. 
