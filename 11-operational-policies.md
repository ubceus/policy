---
title: "11. Operational Policies"
weight: 11
draft: false
---

## 11.1 Awards
### 11.1.1 Presidential Awards
1. The Society shall give out Presidential Awards, which are awards from the President, each year to persons holding official positions within the Society.
    1. The incoming Executives shall receive an "e" patch.
    1. The outgoing Directors shall receive a “d” patch.
    1. The outgoing Managers shall receive an "m" patch.
    1. The outgoing Representatives shall receive an “x” patch.
1. The outgoing Executive shall receive a Gold "Cairn" pin after one full year of service or at the discretion of the President.

### 11.1.2 Executive Awards
The Society shall give out Executive Awards, which are awards from the Executive, each year in recognition of service rendered to the Society by its members, other than in the field of sports. The EUS Executive Awards Committee as outlined by Executive Awards Committee Section 7.2.7 shall decide the Executive Awards.

1. Joey Uyesugi Award
    1. The Joey Uyesugi Award is given in recognition of exceptional dedication and service by an individual, from whom it was not expected.
    1. No more than one shall be awarded each year.
    1. The recipient shall typically be an upper year student who, for the year in which the award is to be made, became extremely involved in the Society.
    1. This award shall be given at the Engineer’s Ball.

1. Spirit Award
    1. This award is given in recognition of the recipient's exceptional ability to motivate their peers.
    1. No more than one Spirit Award shall be awarded each year.
    1. This award is not to be confused with the E-WEEK Spirit Award as outlined in the E-WEEK Section 12.2.
    1. This award shall be given at EUS Awards Night.
1. The Bowinn Ma Award
    1. This award is given in recognition of exceptional dedication, service and self-sacrifice by an individual or individuals in completing their duties, both within and beyond the purview of their position(s).
    1. No more than one award shall be awarded each year.
    1. The recipient shall be a member of the EUS Council who has shown exemplary service on behalf of the society, their program club or their ex-officio club.
    1. This award shall be given at the Engineer’s Ball.
1. Emblem Awards [Long-Term Service Awards]
    1. These awards are awarded for service beyond that called for by the office held by the recipient, and shall take into consideration their record of service for the entire time that they have been a member of the Society.
    1. No more than ten (10) Emblem Awards shall be awarded each year except at the discretion of Executive Awards Committee.
    1. The recipient shall be in their graduating year.
    1. This award shall be given at the Engineer’s Ball.
1. Certificate Awards [One-Year Service Awards]
    1. These awards are to be awarded for service beyond that called for by the office held by the recipient.
    1. No more than fifteen (15) Certificate Awards shall be awarded at the discretion of the Executive Awards Committee.
    1. Certificates shall take into consideration only the record of service for the year in which the award is to be made.
    1. All engineering students shall be eligible for the Certificate awards.
    1. The award shall be presented in two distinctions: with Gratitude and for Dedication.
        1. Certificates with Gratitude
            1. This certificate is awarded to those Society members showing a year of involved service.
            1. No more than twelve (12) Certificates with Gratitude shall be awarded each year, except at the discretion of the Executive Awards Committee.
        1. Certificates for Dedication
            1. The award is for Society members who continually go beyond what is required of them by their office.
            1. No more than three (3) Certificates for Dedication shall be awarded each year, except at the discretion of the Executive Awards Committee.

### 11.1.3 Sports Awards
Sports Awards shall be made by the Sports Director of the Society on the basis of accumulated sports points.

1. Sports points are accumulated from year to year.
1. Sports points shall be awarded according to the sports points criteria sheet, available online. 
    1. Additional points may be awarded at the discretion of the Sports Director.
1. The Sports Awards are:
    1. Small Red “E” Patch
        1. A small red "E" patch shall be awarded to those students accumulating 25 sports points.
    1. Large Red “E” Patch
        1. A large red "E" patch shall be awarded to those students accumulating 50 points.
    1. Gold “E” Patch
        1. A gold "E" patch shall be awarded to those students accumulating 100 points.
    1. White “E” Patch
        1. A white "E" patch shall be awarded to those students accumulating 200 points.

### 11.1.4 Charity Awards
Charity Awards shall be made by the Charity Director of the Society on the basis of accumulated charity points.

1. Charity points are accumulated from year to year.
1. Charity points shall be awarded according to the charity points criteria sheet, available online.
    1. Additional points may be awarded at the discretion of the Charity Director.
1. The Charity Awards are:
    1. Large Charity Star Patch
        1. A large star patch shall be awarded to those students accumulating 30 charity points.
    1. Small Charity Star Patch
        1. Small star patches shall be awarded to those students accumulating 15 charity points following the initial 30, for a maximum of 6 small stars
    1. Charity 123 Patch
        1. A charity 123 patch shall be awarded to those students accumulating 123 charity points.

### 11.1.5 Honorary President
Every year the EUS Executive will chose an Honorary President. The Honorary President shall be an individual who has given great mentorship and support to the EUS and it’s Executives. 

The Honorary President shall:

1. Be invited as an Honoured Guest and Speaker to the Engineer’s Ball
1. Be invited to open E-WEEK at Opening Ceremonies
1. Be invited to other events and receive other honours as decided by the Executive. 

### 11.1.6 Gifts
1. Engineer’s Ball Tickets
    1. One free ticket shall be gratuitously given to the following members:
        1. One representative of each Competing E-WEEK Team.
        1. EUS Executive Officers.
        1. EUS AMS Representative(s).
        1. EUS APSC Student Senator.
    1. Tickets will be distributed subject to the following requirements:
        1. Each of the members listed above must attend at least 75% of all EUS Council Meetings
        1. The ticket will be gifted to the representative who attends the most meetings. 
        1. If more than one representative per competing team meets these requirements, it shall be presented to the President of that Program or Ex-Officio Club.
    1. Additional tickets may be distributed to volunteers who contribute to a predetermined number of events at E-WEEK. This is to be approved by Board prior to distribution.
    1. The cost of all gifted tickets shall be included in the final E-WEEK budget.
1. Volunteer Gift
    1. The Executive shall thank all EUS volunteers with gifts at EUS Awards Night. The gifts shall be based on the amount budgeted. 

## 11.2 Bookings
### 11.2.1 Engineering Student Centre Bookings
The following policy outlines the bookings guidelines that are developed and updated as necessary by the EUS VP Admin for the ESC. 

1. The ESC Bookings Policy shall recognize three different groups of renters:
    1. Internal EUS Groups; which include all Program Clubs & recognized Ex-Officio Clubs
    1. UBC Groups; which include all UBC-affiliated, including alumni groups that are not considered “Internal EUS”. This also includes some Engineering Groups that have not been recognized by the EUS officially. 
    1. External Groups; which include all other parties. 
1. Bookings of the ESC shall be made easiest and preferably free to Internal EUS Groups, then UBC Groups and finally External Groups.
1. Bookings shall be made through the VP Administration or other Bookings Representatives as designated. 
1. Bookings that interfere with access by students to the facilities during regular ESC hours are to be discouraged.

### 11.2.2 Other Bookings
The following policy outlines the bookings guidelines that are developed and updated as necessary by the EUS VP Admin. 

1. The EUS shall aid Program and Ex-Officio Clubs in bookings in other buildings at UBC.
1. The EUS has no control over these bookings policies. 

## 11.3 Communications
### 11.3.1 Description
This policy governs the Society’s official communications. 

### 11.3.2 Website 
The EUS website acts as a source of information for UBC Engineering and those interested in our Faculty. With that in mind, the following points should be followed with respect to the website:

1. Language should be clean. 
1. If requested by any party, specified images must be removed promptly and without question. 

The website should have an up to date repository of all EUS documents (Constitution, Policy Manual, Meeting Minutes, etc.) and event news. 

### 11.3.3 Social Media
EUS Social Media is a source of information and branding for the Society. The following points should be followed with respect to the EUS Social Media:

1. Social Media should link back to EUS Website whenever possible.
1. Facebook
    1. Promotion from the Facebook Page shall be restricted to any EUS or constituency events. 
    1. EUS Executive shall monitor the Facebook Groups for spam and harassment. 
1. Twitter
    1. Promotion from the UBC Engineers Twitter shall be any EUS, Program Club or Ex-Officio Events.
1. Instagram
    1. Promotion from the UBC Engineers Instagram account shall be restricted to EUS Events only. 
1. Snapchat
    1. Promotion from the UBC Engineers Snapchat shall be restricted to EUS Events only. 

## 11.4 Equity
### 11.4.1 Description
Harassment is not tolerated whatsoever by the Society. The Society works to promote equity and well being at all its events, services and members. This topic is extremely sensitive and must be dealt with the utmost care. This policy is subject to interpretation if not applicable to the situation. 

### 11.4.2 Harassment
1. If an officer of the society becomes aware of harassment by a member of the society and/or towards a member of the society, he or she shall immediately report the incident to the President and the AMS Ombudsperson.  
1. Harassment is generally confidential and is not to be discussed with anyone other than is necessary. 
1. The preference shall be to use AMS or UBC Resources such as the AMS Ombudsperson, UBC Ombudsperson, UBC Equity and Inclusion Office or the AMS SASC as applicable.  
1. The choice to pursue a complaint rests with the alleged victim; however, the Society’s policy shall be to prevent the situation occurring again.

### 11.4.3 Volunteer
1. It is expected that all Appointed Volunteers sign an acknowledgement of their responsibility to represent the EUS professionally and create a respectful environment at UBC. 
1. Week E^0 Volunteers shall have additional guidelines as outlined in Week E^0 Policy Section 12.1. 

### 11.4.4 Feedback
1. If at any time, any member or volunteer of the Society feels actions or procedures of the EUS are un-equitable, they should be able to voice this feedback and concern. 
1. The EUS shall have an anonymous feedback form available online for these scenarios. 
1. All official complaints and feedback should go through the official AMS and UBC resources listed above. 

## 11.5 Alcohol
### 11.5.1 Description
The EUS aims to promote a healthy and safe environment to all its members including at events that include alcohol. This policy does not override any documents or laws set by the Alma Mater Society, University of British Columbia, the Governments of British Columbia or Canada. 

### 11.5.2 Applicability
This policy is applicable to the EUS Executive, Directors, Managers, Representatives, and General Volunteers, hereby referred to as “volunteers”.

### 11.5.3 Principles
The following principles shall be upheld:

1. In the case where an individual is chosen by the society to represent the EUS they may not consume alcohol unless it is specified as part of the event.
1. Alcohol use resulting in a risk to the safety of oneself or others can lead to disciplinary action, up to and including removal from elected office as per point 11.5.5.2-11.5.5.3.
1. Pressure should not be applied to others to drink alcohol. 
1. The EUS shall provide a non-alcoholic alternatives and food during all social events where alcohol is served. This policy will be strongly encouraged for any non-EUS group seeking to host a licensed event in the ESC.

### 11.5.4 EUS Events
The following policy governs the conduct of EUS volunteers at EUS-hosted events:

1. Volunteers who are responsible for an event are prohibited from the consumption of alcohol for the duration of the event, including both set-up and take-down.Volunteers who are not in charge of the event may consume alcohol responsibly, recognizing that they represent the EUS. 
1. If a volunteer not in charge is drinking irresponsibly, it is the responsibility of the volunteers in charge of the event to alert the individual in question of their conduct and report the incident to their supervisor.

### 11.5.5 Breaches in the Alcohol Policy
This section governs how the EUS shall respond to breaches of this Alcohol Policy:

1. Any breach of this policy must be reported at the next EUS Council Meeting. If the breach was by a member of the EUS Executive, the incident shall be reported by the Executive Member and the witness. If the breach was by another EUS volunteer, the incident shall be reported by the overseeing Executive and if applicable, the witness.
1. If two incidents occur within the same elected term for the same Executive member and Council deems them both to be in violation of 11.5.3.1-11.5.3.3 and/or 11.5.4.1-11.5.4.3, then Council must conduct a vote of removal of the Executive in question. 
1. If two incidents occur within the same term for the same non-Executive volunteer, the Executive Council must conduct a vote to remove the volunteer in question in accordance with EUS Policy 9.1.5.

